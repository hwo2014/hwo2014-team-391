using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

public class BotFactory
{
    public static Driver CreateBot(string type)
    {
        switch(type)
        {
            case "Mummo": return new Driver();
            case "Herring": return new DriverHerring();
            default: return new DriverHerring();
        }
    }

    public static List<string> BotTypes()
    {
        List<string> l = new List<string>();
        l.Add("BotFromMaster");
        l.Add("BotWithDecisions");
        l.Add("Default");
        return l;
    }
}

public class Driver
{
    public virtual SendMsg Update(FullRace gRace, int gameTick) { return new Throttle(0.6); }
}

public class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public class CarId
{
    public string name;
    public string color;
}

public class GameInit
{
    public Race race;
}

public class Race
{
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;

    //NO JSON
    public double fullLength = 0.0;

    public void InitRace()
    {
        foreach (var piece in track.pieces)
        {
            if (piece.length != 0.0)
            {
                fullLength += piece.length;
            }
            else
            {
                double radius = piece.radius;
                double circlePercent = Math.Abs(piece.angle) / 360.0;
                fullLength += 2.0 * Math.PI * radius * circlePercent;
            }
        }
    }

    public double Distance(CarPosition prev, CarPosition next)
    {
        int currentIndex = prev.piecePosition.pieceIndex;
        double inPiece = prev.piecePosition.inPieceDistance;
        double distance = 0.0;

        if (prev.piecePosition.pieceIndex > next.piecePosition.pieceIndex)
        {
            distance += fullLength * (next.piecePosition.lap - prev.piecePosition.lap);
        }

        if (prev.piecePosition.pieceIndex == next.piecePosition.pieceIndex)
        {
            distance += (next.piecePosition.inPieceDistance - prev.piecePosition.inPieceDistance);
            return distance;
        }

        while (currentIndex != next.piecePosition.pieceIndex)
        {
            double length = 0.0;

            if (track.pieces[currentIndex].length != 0.0)
            {
                length = track.pieces[currentIndex].length;
            }
            else
            {
                double radius = track.pieces[currentIndex].radius;
                double circlePercent = Math.Abs(track.pieces[currentIndex].angle) / 360.0;
                length = 2.0 * Math.PI * radius * circlePercent;
            }

            distance += (length - inPiece);
            inPiece = 0.0;
            ++currentIndex;
            currentIndex %= track.pieces.Length;
        }

        distance += next.piecePosition.inPieceDistance;
        return distance;
    }
}

public class Track
{
    public string id;
    public string name;
    public Piece[] pieces;
    public Lane[] lanes;
}

public class Piece
{
    public double length;

    [JsonProperty(PropertyName = "switch")]
    public bool switcher;
    public double radius;
    public double angle;
}

public class Lane
{
    public double distanceFromCenter;
    public int index;
}

public class StartingPoint
{
    public Position position;
    public double angle;
}

public class Position
{
    public double x;
    public double y;
}

public class Car
{
    public CarId id;
    public Dimension dimensions;
}

public class Dimension
{
    public double length;
    public double width;
    public double guideFlagPosition;
}

public class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace = true;
}

public class CarPositions : MsgWrapper
{
    public new CarPosition[] data;
    public string gameId;
    public int gameTick;

    public CarPositions(string msgType, CarPosition[] data)
        : base(msgType, data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public class CrashSpawn
{
    public string name;
    public string color;
}

public class TurboMessage
{
    public string name;
    public string color;
}

public class CarPosition
{
    public CarId id;
    public double angle;
    public PiecePosition piecePosition;

    public static int Compare(CarPosition pos1, CarPosition pos2)
    {
        int res = 0;

        if (pos1.piecePosition.lap > pos2.piecePosition.lap)
        {
            res = 1;
        }
        else if (pos2.piecePosition.lap > pos1.piecePosition.lap)
        {
            res = -1;
        }
        else if (pos1.piecePosition.pieceIndex > pos2.piecePosition.pieceIndex)
        {
            res = 1;
        }
        else if (pos2.piecePosition.pieceIndex > pos1.piecePosition.pieceIndex)
        {
            res = -1;
        }
        else if (pos1.piecePosition.inPieceDistance > pos2.piecePosition.inPieceDistance)
        {
            res = 1;
        }
        else if (pos2.piecePosition.inPieceDistance > pos1.piecePosition.inPieceDistance)
        {
            res = -1;
        }

        return res;
    }
}

public class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public PieceLane lane;
    public int lap;
}

public class PieceLane
{
    public int startLaneIndex;
    public int endLaneIndex;
}

public class LapFinished
{
    public CarId car;
    public LapTime lapTime;
    public RaceTime raceTime;
    public Ranking ranking;
}

public class TurboData
{
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}

public class LapTime
{
    public int lap;
    public int ticks;
    public int millis;
}

public class RaceTime
{
    public int laps;
    public int ticks;
    public int millis;
}

public class Ranking
{
    public int overall;
    public int fastestLap;
}

abstract public class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    public abstract string MsgType();
}

public class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    public override string MsgType()
    {
        return "join";
    }
}

public class BotId
{
    public string name;
    public string key;

    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}

public class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public int carCount;
    public string password;

    public JoinRace(string name, string key, string circuit, int carCount = 1, string password = "")
    {
        botId = new BotId(name, key);
        this.trackName = circuit;
        this.carCount = carCount;
        this.password = password;
    }

    public override string MsgType()
    {
        return "joinRace";
    }
}

public class CreateRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public int carCount;
    public string password;

    public CreateRace(string name, string key, string circuit, int carCount = 1, string password = "")
    {
        botId = new BotId(name, key);
        this.trackName = circuit;
        this.carCount = carCount;
        this.password = password;
    }

    public override string MsgType()
    {
        return "createRace";
    }
}

public class Ping : SendMsg
{
    public override string MsgType()
    {
        return "ping";
    }
}

public class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    public override string MsgType()
    {
        return "throttle";
    }
}

public class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    public static SwitchLane SwitchLeft()
    {
        return new SwitchLane("Left");
    }

    public static SwitchLane SwitchRight()
    {
        return new SwitchLane("Right");
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    public override string MsgType()
    {
        return "switchLane";
    }
}

public class Turbo : SendMsg
{
    public string value;

    public Turbo(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    public override string MsgType()
    {
        return "turbo";
    }
}


public class Bot 
{
    public delegate void PlotData(string botName, Car car, CarDash dash, CarPosition position, Track track, int tick, string inputMsg, SendMsg response, LookAhead lookAhead);

    public delegate void RaceStart(GameInit data);

    public delegate void LapCompleted(string botName, LapFinished lap);

    public static PlotData Plotter;

    public static RaceStart RaceStartFunc;

    public static LapCompleted LapCompletedFunc;

	public static void Main(string[] args) 
    {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        string circuit = "";
        bool create = false;
        int bots = 1;
        string password = "shallnot";
        string botEngineType = "";

        if (args.Length >= 5)
        {
            circuit = args[4];
        }
       
        if(args.Length >= 6)
        {
            bots = Convert.ToInt32(args[5]);
        }

        if (args.Length >= 7)
        {
            password = args[6];
        }

        if (args.Length >= 8)
        {
            botEngineType = args[7];
        }

        var botEngine = BotFactory.CreateBot(botEngineType);
       
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) 
        {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

            if(create)
            {
                new Bot(reader, writer, new CreateRace(botName, botKey, circuit, bots, password), botName, botEngine);
            }
            else if(circuit.Length > 0)
            {
                new Bot(reader, writer, new JoinRace(botName, botKey, circuit, bots), botName, botEngine);
            }
            else
            {
                new Bot(reader, writer, new Join(botName, botKey), botName, botEngine);
            }
		}
	}

	private StreamWriter writer;

    public static bool Exit = false;
    public static int joinCount = 0;

    string _botName = "";
    GameInit gInit = null;
    int gameTick = -1;
    FullRace gRace;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join, string botName, Driver driver) 
    {
        gRace = new FullRace();
        gRace.players = new Players();

        _botName = botName;
		this.writer = writer;
		string line;

		send(join);

        try
        { 
            while ((line = reader.ReadLine()) != null && !Exit) 
            {
			    MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                SendMsg response = null;

			    switch(msg.msgType) 
                {
				    case "carPositions":
                        CarPositions cp = JsonConvert.DeserializeObject<CarPositions>(line);
                        gameTick = cp.gameTick;
                        gRace.players.NewPosition(cp.data, gameTick);

                        gRace.players.me.UpdateLeaderboard();

                        response = driver.Update(gRace, gameTick);

					    break;
				    case "join":
					    Console.WriteLine("Joined");
                        ++joinCount;
					    break;
                    case "joinRace":
                        ++joinCount;
                        break;
				    case "gameInit":
                        gInit = JsonConvert.DeserializeObject<GameInit>(msg.data.ToString());

                        gInit.race.InitRace();
                        gRace.laps = gInit.race.raceSession.laps;
                        gRace.isQualifying = gInit.race.raceSession.quickRace;
                        if(gRace.isQualifying)
                        {
                            Player.GeeFactor = 1.0;
                        }
                        gRace.track = new FullTrack(gInit.race.track, gInit.race.track.name, gRace);
                        gRace.players.AddCars(gInit.race.cars);
                        gRace.players.Init(gRace);
                        
                        if(RaceStartFunc != null)
                        {
                            RaceStartFunc(gInit);
                        }

					    break;
				    case "gameEnd":
                        if (gRace != null &&
                           gRace.track != null)
                        {
                            gRace.track.SaveProbe();
                        }

					    break;
				    case "gameStart":
                        if(gRace != null &&
                            gRace.track != null)
                        {
                            gRace.track.LoadProbe();
                        }

					    break;
                    case "yourCar":
                        CarId carId = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                        gRace.players.AddMe(carId);
                        break;

                    case "lapFinished":
                        var lapFinished = JsonConvert.DeserializeObject<LapFinished>(msg.data.ToString());

                        if(lapFinished != null)
                        {
                            if (LapCompletedFunc != null)
                            {
                                if (lapFinished.car.color == gRace.players.me.Id)
                                {
                                    LapCompletedFunc(_botName, lapFinished);
                                }
                            }
                        }
                    
                        break;

                    case "crash":
                        var crashData = JsonConvert.DeserializeObject<CrashSpawn>(msg.data.ToString());
                        gRace.players.Crashed(crashData);
                        break;

                    case "spawn":
                        var spawnData = JsonConvert.DeserializeObject<CrashSpawn>(msg.data.ToString());
                        gRace.players.Spawned(spawnData);
                        break;

                    case "turboAvailable":
                        var turboData = JsonConvert.DeserializeObject<TurboData>(msg.data.ToString());
                        gRace.players.TurboAvailable(turboData);
                        Console.WriteLine("TURBO: " + turboData.turboFactor.ToString());
                        break;

                    case "turboStart":
                        var turboOn = JsonConvert.DeserializeObject<TurboMessage>(msg.data.ToString());
                        gRace.players.TurboOn(turboOn);
                        break;

                    case "turboEnd":
                        var turboOff = JsonConvert.DeserializeObject<TurboMessage>(msg.data.ToString());
                        gRace.players.TurboOff(turboOff);
                        break;

                    case "finish":
                        break;

                    case "tournamentEnd":
                        Exit = true;
                        break;

				    default:
                        Console.WriteLine("Unk: " + msg.msgType);
					    break;
			    }

                if (response == null)
                {
                    response = new Ping();
                }

                send(response);

                if (Plotter != null)
                {
                    var me = gRace.players.me;

                    if(me != null && me.car != null)
                    {
                        Plotter(_botName,
                            me.car,
                            me.dash,
                            me.position,
                            gInit.race.track,
                            gameTick,
                            msg.msgType,
                            response,
                            me.lookahead);
                    }
                }
		    }
        }
        catch(Exception)
        {

        }

#if DEBUG
        while(!Exit)
        {
            Thread.Sleep(100);
        }
#endif

        --joinCount;
	}

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

//individual track piece lookahead data
public class LookAheadItemData
{
    public double TargetSpeed { get; set; }
    public double Distance { get; set; }
    public int TargetTrackId { get; set; }
    public double TrackPieceLength { get; set; }
}

public class LookAhead
{
    FullTrack _track;
    double _targetSpeed;
    double _distance;
    int _targetTrackId;

    public Queue<LookAheadItemData> Datas { get; set; }    
    
    public LookAhead(FullTrack track)
    {
        _track = track;
        Datas = new Queue<LookAheadItemData>(5);
    }

    public double TargetSpeed
    {
        get
        {
            return _targetSpeed;
        }
        set
        {
            _targetSpeed = value;
        }
    }

    public double Distance
    {
        get
        {
            return _distance;
        }
        set
        {
            _distance = value;
        }
    }

    public int TargetTrackId
    {
        get
        {
            return _targetTrackId;
        }
        set
        {
            _targetTrackId = value;
        }
    }

    public void Update(Player player)
    {
        //var lookAhead = Range(_track.parts, player.position.piecePosition.pieceIndex, 5);
        double slowestTargetSpeed = 1000;
        double distanceToTargetSpeed = 1000;
        int nextIndex = player.position.piecePosition.pieceIndex;
        double smallestDistanceToBrake = 1000;
        
        // start with one after current piece (it's lookahead for a reason)
        ++nextIndex;
        double pieceDistanceLeft = (_track.parts[player.position.piecePosition.pieceIndex].Length(player.position.piecePosition.lane) -
            player.position.piecePosition.inPieceDistance);

        //Console.WriteLine("pieceDistanceLeft: " + pieceDistanceLeft);
        double distanceToTarget = pieceDistanceLeft;
        Datas.Clear();

        //Changed 5 to 8 to compensate for Turbo speeds...
        //TODO: Maybe we should get enough distance for current speed...
        for (int i = 0; i < 8; i++)
        {
            var next = _track.SafeAt(nextIndex);
            var pieceSpeedLimit = player.SpeedLimit(next.index, player.position.piecePosition.lane.startLaneIndex, player.position.piecePosition.lane.endLaneIndex);
            LookAheadItemData data = new LookAheadItemData()
            {
                Distance = distanceToTarget,
                TargetSpeed = pieceSpeedLimit,
                TargetTrackId = next.index,
                TrackPieceLength = _track.PieceLength(next.index, player.position.piecePosition.lane.startLaneIndex, player.position.piecePosition.lane.endLaneIndex)
            };
            Datas.Enqueue(data);
            
            double brakeDistance = player.dash.BrakeDistance(player.dash.Speed + player.dash.Acceleration * 2, pieceSpeedLimit, 0.0);

            if ((distanceToTarget - brakeDistance) < smallestDistanceToBrake)
            //if (distanceToTarget < distanceToTargetSpeed) //Worked for germany, but not with turbo...
            //if (pieceSpeedLimit < slowestTargetSpeed) //Did not work for germany
            {
                slowestTargetSpeed = pieceSpeedLimit;
                distanceToTargetSpeed = distanceToTarget;
                _targetTrackId = nextIndex;
                smallestDistanceToBrake = (distanceToTarget - brakeDistance);
            }

            distanceToTarget += _track.PieceLength(next.index, player.position.piecePosition.lane.startLaneIndex, player.position.piecePosition.lane.endLaneIndex);
            nextIndex++;
        }
        // now we have all 5 data points set up.
        // Calculate curve impulse. Short bends have less impulse than long bends  (I = F  x dt)
        // Current trivial approach just ignores short bends
        // todo: calculate time spent in a curve (dt) and impact based on that.
        LookAheadItemData currentItem = null;

        // find first non straight item
        int index = 0;
        LookAheadItemData[] datas = Datas.ToArray();
        while (index < datas.Length)
        {
            currentItem = datas[index];
            if (currentItem.TargetSpeed > 100)
            {
                index++;
            }
            else
            {
                break;
            }
        }
        // only straights in lookahead data - full speed
        if (index == datas.Length)
        {
            _targetSpeed = slowestTargetSpeed;
            _distance = distanceToTargetSpeed;
        }
        else
        {
            // curves ahead, adjust throttle according to impulse
            int impact = 0;
            double straights = 0.0;

            while (index < datas.Length)
            {
                currentItem = datas[index];
                if (currentItem.TargetSpeed > 100)
                {
                    straights += currentItem.TrackPieceLength;

                    if (straights > 200)
                    {
                        break;
                    }
                }
                else
                {
                    straights = 0.0;
                    impact++;
                }
                index++;
            }
            // short curve - full throttle
            if (impact <= 1)
            {
                _targetSpeed = 100;
                _distance = smallestDistanceToBrake;
            }
            // long curve - limit speed
            else
            {
                _targetSpeed = slowestTargetSpeed;
                _distance = smallestDistanceToBrake;
            }
        }
    }

    private List<TrackPart> Range(List<TrackPart> pieces, int startIndex, int count)
    {
        List<TrackPart> batch = new List<TrackPart>();
        for (int i = startIndex; i < startIndex + count; i++)
        {
            batch.Add(pieces[i % pieces.Count]);
        }
        return batch;
    }

    internal LookAhead Copy()
    {
        LookAhead copy = new LookAhead(_track)
        {
            _distance = this._distance,
            _targetSpeed = this._targetSpeed,
            _targetTrackId = this._targetTrackId,
            Datas = new Queue<LookAheadItemData>(Datas)
        };
        return copy;
    }
}

public class CarDash
{
    public enum TurboState
    {
        Turbo_OFF,
        Turbo_Available,
        Turbo_ON
    }

    FullTrack _track;
    int _lastPiece = -1;
    int _lastTick;
    double _lastPiecePos;
    double _speed;
    double _angularVelocity;
    double _lastAngle;
    double _previousAngularVelocity;
    double _previousAngularAcceleration;
    double _angularAcceleration;
    double _carinertia;
    double _carTorque;
    double _largestAngle = 0.0;
    double _bestAcceleration = 0.0;
    double _bestDeceleration = 0.0;
    double _acceleration = 0.0;
    double _topSpeed = 0.0;
    double _lastCalculatedAcc = 0.0;
    double _throttle = 0.0;
    double _currentGee = 0.0;
    double _minGee = 0.0;
    double _maxGee = 0.0;
    double _carPower = 10.0;
    double _carPowerMultiplier = 1.0;
    bool _alive = true;
    double _measuredBrakeDeceleration = 0.98;
    static double _measuredSlipGee = 293;
    TurboState _turboState = TurboState.Turbo_OFF;

    public CarDash(FullTrack track)
    {
        _track = track;
        _speed = 0.0;
        _lastTick = -1;
    }

    public CarDash Copy()
    {
        CarDash d = new CarDash(_track);
        d._lastAngle = _lastAngle;
        d._acceleration = _acceleration;
        d._alive = _alive;
        d._angularVelocity = _angularVelocity;
        d._angularAcceleration = _angularAcceleration;
        d._carinertia = _carinertia;
        d._carTorque = _carTorque;
        d._previousAngularAcceleration = _previousAngularAcceleration;
        d._previousAngularVelocity = _previousAngularVelocity;
        d._bestAcceleration = _bestAcceleration;
        d._bestDeceleration = _bestDeceleration;
        d._currentGee = _currentGee;
        d._largestAngle = _largestAngle;
        d._lastAngle = _lastAngle;
        d._lastCalculatedAcc = _lastCalculatedAcc;
        d._lastPiece = _lastPiece;
        d._lastPiecePos = _lastPiecePos;
        d._lastTick = _lastTick;
        d._maxGee = _maxGee;
        d._minGee = _minGee;
        d._throttle = _throttle;
        d._speed = _speed;
        d._topSpeed = _topSpeed;
        d._carPower = _carPower;
        d._carPowerMultiplier = _carPowerMultiplier;
        d._turboState = _turboState;
        d._measuredBrakeDeceleration = _measuredBrakeDeceleration;
  
        return d;
    }

    public bool Alive
    {
        get
        {
            return _alive;
        }
    }

    public double Gee
    {
        get
        {
            return _currentGee;
        }
    }

    public double GeeMin
    {
        get
        {
            return _minGee;
        }
    }

    public double GeeMax
    {
        get
        {
            return _maxGee;
        }
    }

    public double Acceleration
    {
        get
        {
            return _acceleration;
        }
    }

    public double Throttle
    {
        get
        {
            return _throttle;
        }
        set
        {
            _throttle = value;
        }
    }

    public TurboState Turbo
    {
        get
        {
            return _turboState;
        }
        set
        {
            _turboState = value;
        }
    }

    public static double GetNextSpeed(double withThrottle, double currentSpeed)
    {
        double acc = withThrottle * 0.2 - (currentSpeed / 10.0 * 0.2);
        return currentSpeed + acc;
    }

    public double MaxSpeedAfter(double currentSpeed, double distance, double withThrottle)
    {
        while(distance > 0.0)
        {
            double acc = withThrottle * 0.2 - (currentSpeed / 10.0 * 0.2);
            distance -= currentSpeed;
            currentSpeed += acc;
        }

        return currentSpeed;
    }

    public double BrakeDistance(double currentSpeed, double targetSpeed, double withThrottle)
    {
        double distance = 0.0;

        while (currentSpeed > targetSpeed)
        {
            //original code was:
            double accOrig = withThrottle * 0.2 - (currentSpeed / 10.0 * 0.2);
            //use measuredDv instead of hardcoded 0.02
            //double acc = withThrottle * 10* MeasuredBrakeDecelaration - (currentSpeed * MeasuredBrakeDecelaration);
            double acc = - (currentSpeed * MeasuredBrakeDecelaration);
            //Console.WriteLine("orig: " + accOrig);
            //Console.WriteLine("acc: " + acc);
            distance += currentSpeed;
            currentSpeed += acc;
        }

        return distance;
    }

    public double Speed
    {
        get
        {
            return _speed;
        }
    }

    public double AngularVelocity
    {
        get
        {
            return _angularVelocity;
        }
    }

    public double AngularAcceleration
    {
        get
        {
            return _angularAcceleration;
        }
    }

    public double CarInertia
    {
        get
        {
            return _carinertia;
        }
    }

    public double CarTorque
    {
        get
        {
            return _carTorque;
        }
    }

    public double CarPower
    {
        get
        {
            return _carPower * _carPowerMultiplier;
        }
        set
        {
            _carPower = value;
        }
    }

    public double CarPowerMultiplier
    {
        get
        {
            return _carPowerMultiplier;
        }
        set
        {
            _carPowerMultiplier = value;
        }
    }

    public double LastAngle
    {
        get
        {
            return _lastAngle;
        }
    }

    public void Crashed()
    {
        _alive = false;
    }

    public void Spawned()
    {
        _alive = true;
    }

    public double MeasuredBrakeDecelaration
    {
        get
        {       
            return _measuredBrakeDeceleration;
        }
        set
        {
            _measuredBrakeDeceleration = value;
        }
    }

    public double MeasuredSlipGee
    {
        get
        {
            return _measuredSlipGee;
        }
        set
        {
            _measuredSlipGee = value;
        }
    }

	public Car Car { get; set; }

    bool powerAnalysed = false;
    
    public void Update(Player player, double speedFromSystem, int gameTick)
    {
        if (_lastPiece == -1)
        {
            _lastPiece = 0;
            _lastPiecePos = 0.0;
            _lastAngle = 0.0;
        }

        if(_lastTick == -1)
        {
            _lastTick = gameTick - 1;
        }

        _previousAngularVelocity = _angularVelocity;
        _previousAngularAcceleration = _angularAcceleration;
        _angularVelocity = (player.position.angle - _lastAngle);
        _angularAcceleration = (_angularVelocity - _previousAngularVelocity);
        _lastAngle = player.position.angle;

        double lastSpeed = _speed;
        int ticks = (gameTick - _lastTick);

        if (Double.IsNaN(speedFromSystem))
        {
            //_speed = GetNextSpeed(_throttle, _speed); 
            //_accurateSpeed = false;
            _speed += _acceleration; 
        }
        else
        {
            _speed = speedFromSystem;
            _acceleration = (_speed - lastSpeed) / ticks;

            if(_throttle == 1.0 && _speed > lastSpeed)
            {
                if(!powerAnalysed)
                {
                    CarPower = 0.2 / _acceleration * 10.0;
                    powerAnalysed = true;
                }
            }
        }

        _topSpeed = Math.Max(_speed, _topSpeed);
        _largestAngle = Math.Max(Math.Abs(_lastAngle), _largestAngle);

        if (_alive)
        {
            if (_acceleration > 0.0)
            {
                _bestAcceleration = Math.Max(_acceleration, _bestAcceleration);
            }
            else
            {
                _bestDeceleration = Math.Min(_acceleration, _bestDeceleration);
            }
        }

        _lastTick = gameTick;
        _lastPiece = player.position.piecePosition.pieceIndex;
        _lastPiecePos = player.position.piecePosition.inPieceDistance;

        _currentGee = GetGeeForce(player.position.piecePosition.pieceIndex, player.position.piecePosition.lane.startLaneIndex,
                                    player.position.piecePosition.lane.endLaneIndex, _speed);
        _carinertia = GetInertia(_currentGee);
        _carTorque = _carinertia *  (Car.dimensions.guideFlagPosition + Car.dimensions.length / 2) / Car.dimensions.length;
        _minGee = Math.Min(_currentGee, _minGee);
        _maxGee = Math.Max(_currentGee, _maxGee);
    }

    public double GetInertia(double geeForce)
    {
        double l2 = Car.dimensions.length / 2;
        double a = l2 - Car.dimensions.guideFlagPosition;

        double inertia =  geeForce * (l2 - a) / l2;
        return inertia;
    }

    public double GetGeeForce(int index, int startLaneIndex, int endLaneIndex, double speed)
    {
        double geeForce = 0.0;
        TrackPart part = _track.SafeAt(index);

        if (part.turn)
        {
            double radius = part.Radius(startLaneIndex, endLaneIndex);
            geeForce = (speed * speed) / radius;

            if (part.angle > 0.0)
            {
                geeForce *= -1.0;
            }
        }

        return geeForce * 1000.0;
    }
}

public class FullRace
{
    public FullTrack track;
    public Players players;
    public int laps;
    public bool isQualifying;
}

public class FullTrack
{
    public FullTrack(Track trk, string trackName, FullRace race)
    {
        this.race = race;
        name = trackName;
        laneCount = trk.lanes.Length;
        Lane[] lanes = new Lane[laneCount];

        foreach(var lane in trk.lanes)
        {
            lanes[lane.index] = lane;
        }

        length = 0.0;

        foreach(var part in trk.pieces)
        {
            var piece = TrackPart.FromPiece(part, parts.Count, lanes);
            length += TrackPart.AverageLength(piece);
            parts.Add(piece);
        }

        foreach(var part in parts)
        {
            part.straightLeft = StraightLeft(part.index);
        }

        parts.Sort(new TrackPart_LengthComparer());
        double lastLength = double.NaN;
        int prior = 0;

        for (int i = 0; i < parts.Count; i++)
        {
            TrackPart part = parts[i];

            if(part.straightLeft >= 200.0)
            {
                if (!double.IsNaN(lastLength) && lastLength != part.straightLeft)
                {
                    ++prior;
                }

                part.turboPriority = prior;
                lastLength = part.straightLeft;
            }
            else
            {
                break;
            }
        }

        parts.Sort(new TrackPart_IndexComparer());

        for (int i = parts.Count - 1; i >= 0; i--)
        {
            if(parts[i].straightLeft > 0.0)
            {
                parts[i].lastStraight = true;
            }
            else
            {
                break;
            }                
        }
    }

    public double StraightLeft(int fromIndex)
    {
        double ret = 0.0;
        TrackPart part = parts[fromIndex];

        while (part != null && !part.turn)
        {
            ret += part.Length();
            part = GetNextPart(part);
        }

        return ret;
    }

    public double PieceLength(int index, int enterLane, int exitLane)
    {
        return parts[index].Length(enterLane, exitLane);
    }

    public double PieceLength(int index, PieceLane lane)
    {
        return parts[index].Length(lane);
    }

    public TrackPart SafeAt(int index)
    {
        while(index < 0)
        {
            index += parts.Count;
        }

        index %= parts.Count;

        return parts[index];
    }

    public int GetNextIndex(int index)
    {
        return (index + 1) % parts.Count;
    }

    public int GetPrevIndex(int index)
    {
        if(--index < 0)
        {
            index += parts.Count;
        }

        return index;
    }

    public TrackPart GetNextPart(TrackPart part)
    {
        return parts[GetNextIndex(part.index)];
    }

    public TrackPart GetPrevPart(TrackPart part)
    {
        return parts[GetPrevIndex(part.index)];
    }

    public double Move(Player player, CarPosition to)
    {
        double distance = 0.0;
        TrackPart takeFrom = null;
        TrackPart putTo = null;

        if(player.position != null)
        {
            int fromIndex = player.position.piecePosition.pieceIndex;
            takeFrom = parts[fromIndex];

            if(to != null)
            {
                int toIndex = to.piecePosition.pieceIndex;

                if(fromIndex == toIndex)
                {
                    distance = to.piecePosition.inPieceDistance - player.position.piecePosition.inPieceDistance;
                    takeFrom = null;
                }
                else
                {
                    putTo = parts[to.piecePosition.pieceIndex];

                    if(takeFrom.switcher && !putTo.switcher)
                    {
                        double swLen = player.position.piecePosition.inPieceDistance;
                        swLen += player.dash.Speed;
                        swLen -= to.piecePosition.inPieceDistance;

                        //Console.WriteLine("SwLen: " + swLen.ToString() +
                        //                    "Ind: " + player.position.piecePosition.pieceIndex.ToString());
                    }

                    distance = takeFrom.Length(player.position.piecePosition.lane.startLaneIndex,
                                                    player.position.piecePosition.lane.endLaneIndex);

                    distance -= player.position.piecePosition.inPieceDistance;

                    fromIndex = GetNextIndex(fromIndex);

                    while(fromIndex != toIndex)
                    {
                        distance += parts[fromIndex].Length(to.piecePosition.lane.startLaneIndex,
                                                            to.piecePosition.lane.startLaneIndex);

                        fromIndex = GetNextIndex(fromIndex);
                    }

                    distance += to.piecePosition.inPieceDistance;
                }
            }
        }
        else if(to != null)
        {
            putTo = parts[to.piecePosition.pieceIndex];
        }

        player.position = to;

        if (takeFrom != null)
        {
            takeFrom.players.Remove(player.Id);
        }

        if(putTo != null)
        {
            putTo.players.Add(player.Id, player);
        }

        return distance;
    }

    public double ForwardDistanceBetween(PiecePosition p1, PiecePosition p2)
    {
        double ret = 0.0;

        int currentIndex = p1.pieceIndex;
        int targetIndex = p2.pieceIndex;

        if(currentIndex == targetIndex)
        {
            return p1.inPieceDistance - p2.inPieceDistance;
        }
        else
        {
            ret = PieceLength(currentIndex, p1.lane) - p1.inPieceDistance;
            int lane = p1.lane.endLaneIndex;
            currentIndex = GetNextIndex(currentIndex);

            while(currentIndex != targetIndex)
            {
                ret += PieceLength(currentIndex, lane, lane);
                currentIndex = GetNextIndex(currentIndex);
            }

            ret += p2.inPieceDistance;
        }

        return ret;
    }

    public double ForwardDistanceBetween(Player p1, Player p2)
    {
        return ForwardDistanceBetween(p1.position.piecePosition, p2.position.piecePosition);
    }

    public void LoadProbe()
    {
        probe = TrackProbe.Create(name, this);
    }

    public void SaveProbe()
    {
        if(probe != null)
        { 
            TrackProbe.Save(probe);
        }
    }

    public void UpgradeMaxGee(int index, double amount = 0.5)
    {
        if(probe != null)
        {
            probe.UpgradeMaxGee(index, amount);
        }
    }

    public List<TrackPart> parts = new List<TrackPart>();
    public double length;
    public int laneCount;
    public TrackProbe probe;
    private string name;
    private FullRace race;
}

public class TrackPart_LengthComparer : IComparer<TrackPart>
{
    public int Compare(TrackPart x, TrackPart y)
    {
        return y.straightLeft.CompareTo(x.straightLeft);
    }
}

public class TrackPart_IndexComparer : IComparer<TrackPart>
{
    public int Compare(TrackPart x, TrackPart y)
    {
        return x.index.CompareTo(y.index);
    }
}

public class TrackPart
{
    public TrackPart(int lanes, int index)
    {
        length = new double[lanes];
        radius = new double[lanes];
        angle = 0.0;
        turn = false;
        switcher = false;
        straightLeft = -1.0;
        this.index = index;
        turboPriority = 1000000;
    }

    public static TrackPart FromPiece(Piece piece, int index, Lane[] lanes)
    {
        TrackPart part = new TrackPart(lanes.Length, index);
        part.switcher = piece.switcher;
        part.lanes = lanes;

        if(piece.angle != 0.0)
        {
            part.turn = true;
            part.angle = piece.angle * Math.PI / 180.0;

            double aangle = Math.Abs(part.angle);

            for (int i = 0; i < lanes.Length; i++)
            {
                if(part.angle < 0.0)
                {
                    part.radius[i] = piece.radius + lanes[i].distanceFromCenter;
                }
                else
                {
                    part.radius[i] = piece.radius - lanes[i].distanceFromCenter;
                }

                part.length[i] = Math.PI * part.radius[i] * aangle / Math.PI;
            }
        }
        else
        {
            for(int i = 0; i < lanes.Length; i++)
            {
                part.length[i] = piece.length;
                part.radius[i] = 0.0;
            }
        }

        return part;
    }

    public static double AverageLength(TrackPart part)
    {
        return part.Length();
    }

    public double Radius(int enterLane, int exitLane)
    {
        return Math.Min(radius[enterLane], radius[exitLane]);
    }

    public double BezierLength(int steps,
                            double x1, double y1,
                            double x2, double y2,
                            double x3, double y3,
                            double x4, double y4)
    {
        double step = 1.0 / (steps + 1);
        double t = step;
        double lx = x1;
        double ly = y1;
        double length = 0.0;

        double difX;
        double difY;

        for(int i = 0; i < steps; i++)
        {
            double t2 = t * t;
            double t3 = t2 * t;

            double x = x1 + (-x1 * 3 + t * (3 * x1 - x1 * t)) * t
                + (3 * x2 + t * (-6 * x2 + x2 * 3 * t)) * t
                + (x3 * 3 - x3 * 3 * t) * t2
                + x4 * t3;

            double y = y1 + (-y1 * 3 + t * (3 * y1 - y1 * t)) * t
                + (3 * y2 + t * (-6 * y2 + y2 * 3 * t)) * t
                + (y3 * 3 - y3 * 3 * t) * t2
                + y4 * t3;

            difX = x - lx;
            difY = y - ly;

            length += Math.Sqrt((difY * difY) + (difX * difX));

            lx = x;
            ly = y;
            t += step;
        }

        difX = x4 - lx;
        difY = y4 - ly;
        length += Math.Sqrt((difY * difY) + (difX * difX));

        return length;
    }

    public double Length(int enterLane, int exitLane)
    {
        if(switcher)
        {
            if (enterLane == exitLane)
            {
                return length[enterLane];
            }
            else if(turn)
            {
                double maxLane = Math.Max(length[enterLane], length[exitLane]);
                double minLane = Math.Min(length[enterLane], length[exitLane]);

                //Dunno how these go...dummy approx
                double approx = (maxLane * 3 + minLane) / 4.0;

                return approx;
            }
            else
            {
                //This should be bezier I guess...
                //Dummy approximation only....
                double laneDistance = Math.Abs(lanes[enterLane].distanceFromCenter - lanes[exitLane].distanceFromCenter);
                double ret = length[enterLane] + laneDistance / 9;
                
                //double ret = Math.Sqrt((laneDistance * laneDistance) + (length[enterLane] * length[enterLane]));
                return ret;
            }
        }
        else
        {
            return length[enterLane];
        }
    }

    public double Length(PieceLane lane)
    {
        return Length(lane.startLaneIndex, lane.endLaneIndex);
    }

    public double Length()
    {
        double total = 0.0;

        for (int i = 0; i < length.Length; i++)
        {
            total += length[i];
        }

        return total / length.Length;
    }

    public List<Player> EnemiesOnLane(int laneIndex, Player me)
    {
        if(laneIndex < 0 || laneIndex >= length.Length || players.Count == 0)
        {
            return null;
        }

        List<Player> ret = new List<Player>();

        foreach (var player in players.Values)
        {
            if(player.position.piecePosition.lane.endLaneIndex == laneIndex &&
                player.Id != me.Id &&
                player.PositionProvided)
            {
                ret.Add(player);
            }
        }

        return ret;
    }

    public int GetLaneIndexAt(int enter, int offset)
    {
        if(offset < -1 || offset > 1)
        {
            return -1;
        }
        else if(offset == 0)
        {
            return enter;
        }

        double currentDistanceFromCenter = lanes[enter].distanceFromCenter;

        if(offset < 0)
        {
            //Do we have smaller distance from center...
            foreach(var lane in lanes)
            {
                if(lane.distanceFromCenter < currentDistanceFromCenter)
                {
                    return lane.index;
                }
            }
        }
        else
        {
            //Do we have larger distance from center...
            foreach (var lane in lanes)
            {
                if (lane.distanceFromCenter > currentDistanceFromCenter)
                {
                    return lane.index;
                }
            }
        }

        return -1;
    }

    public Dictionary<string, Player> players = new Dictionary<string, Player>();
    public int index;
    public bool turn;
    public bool switcher;
    public double[] length;
    public double[] radius;
    public double angle;
    public Lane[] lanes;
    public double straightLeft;
    public int turboPriority;
    public bool lastStraight = false;
}

public class Player
{
    enum RaceState
    {
        Leading,
        LeadingALot,
        Losing,
        LosingALot
    }

    public Player()
    {
        lastMoved = 0.0;
        lastTick = -1;
        myself = false;
        PositionProvided = true;
    }

    public Player(Car car)
    {
        this.car = car;
        Id = car.id.color;
        lastMoved = 0.0;
        myself = false;
    }

    public string Id
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    public bool TurboState
    {
        get
        {
            return turboState;
        }
        set
        {
            if(turboState != value)
            {
                turboState = value;

                if(turboState)
                {
                    if (lastTurbo != null)
                    {
                        dash.CarPowerMultiplier = lastTurbo.turboFactor;
                        dash.Turbo = CarDash.TurboState.Turbo_ON;
                    }
                }
                else
                {
                    if(turboAvailable != null)
                    {
                        dash.Turbo = CarDash.TurboState.Turbo_Available;
                    }
                    else
                    {
                        dash.Turbo = CarDash.TurboState.Turbo_OFF;
                    }
                    
                    dash.CarPowerMultiplier = 1.0;
                }
            }

            turboState = value;
        }
    }

    public void SetCar(Car car)
    {
        this.car = car;
        Id = car.id.color;
    }

    public void Init(FullRace race)
    {
        lookahead = new LookAhead(race.track);
        dash = new CarDash(race.track);
        dash.Car = car;
        intermediates = new int[race.track.parts.Count];

        for (int i = 0; i < race.track.parts.Count; i++ )
        {
            intermediates[i] = int.MaxValue;
        }

        this.race = race;
    }

    public void Crashed()
    {
        crashOccurred = true;
        dash.Crashed();
    }

    public void Spawned()
    {
        measuringTicks = -1;
        dash.Spawned();
    }

    public void NewPosition(CarPosition position, int gameTick)
    {
        if(turboReceived > 0)
        {
            --turboReceived;
        }

        if(measuringTicks == -1)
        {
            measuringTicks = gameTick;
        }

        CarPosition lastPosition = this.position;
        lastMoved = race.track.Move(this, position);

        if (lastPosition != null && lastPosition.piecePosition.lap >= 0)
        {
            if(lastPosition.piecePosition.pieceIndex != position.piecePosition.pieceIndex)
            {
                if(myself)
                {
                    TrackPart last = race.track.SafeAt(lastPosition.piecePosition.pieceIndex);
                    TrackPart curr = race.track.SafeAt(position.piecePosition.pieceIndex);

                    if (last.turn && !curr.turn)
                    {
                        double upgrade = 0.0;

                        if(crashOccurred)
                        {
                            double defLimit = GeeFactor * GeeDefaultLimit();

                            if (maxGeeThroughCorner < defLimit)
                            {
                                double scale = maxGeeThroughCorner / defLimit;
                                GeeFactor = Math.Min(GeeFactor - 0.1, scale * GeeFactor);
                            }
                            else
                            {
                                upgrade = -1.0;
                            }
                        }
                        else if(race.isQualifying)
                        {
                            if (Math.Abs(curr.angle) <= Math.Abs(last.angle))
                            {
                                if (maxAngleThroughCorner < 30.0)
                                {
                                    upgrade = 2;
                                }
                                else if(maxAngleThroughCorner < 45.0)
                                {
                                    upgrade = 1;
                                }
                                else if (maxAngleThroughCorner < 50.0)
                                {
                                    upgrade = 0.2;
                                }
                                else if (maxAngleThroughCorner < 54.0)
                                {
                                    upgrade = 0.1;
                                }
                                else if (maxAngleThroughCorner < 56.0)
                                {
                                    upgrade = 0.05;
                                }
                                else if(maxAngleThroughCorner >= 57)
                                {
                                    upgrade = -0.1;
                                }

                                if(upgrade != 0.0)
                                {
                                    TrackPart tmp = last;
                                    int count = 0;

                                    while (tmp.turn)
                                    {
                                        ++count;
                                        race.track.UpgradeMaxGee(last.index, upgrade);
                                        tmp = race.track.GetPrevPart(tmp);
                                    }

                                    if(count > 0)
                                    {
                                        upgrade /= count;
                                    }
                                }
                            }
                        }

                        if (upgrade != 0.0 && (gameTick - measuringTicks > 150 || crashOccurred))
                        {
                            while (last.turn)
                            {
                                race.track.UpgradeMaxGee(last.index, upgrade);
                                last = race.track.GetPrevPart(last);
                            }
                        }
                        
                        maxAngleThroughCorner = 0.0f;
                        maxSpeedThroughCorner = 0.0f;
                        maxGeeThroughCorner = 0.0;
                        crashOccurred = false;
                        ticksThroughCorner = 0;
                    }
                    
                }

                int prevIMIndex = race.track.GetPrevIndex(position.piecePosition.pieceIndex);
                intermediates[prevIMIndex] = gameTick;
            }
        }

        if (dash != null)
        {
            double accurateSpeed = double.NaN;

            if(lastPosition == null)
            {
                accurateSpeed = position.piecePosition.inPieceDistance / (gameTick - lastTick);
            }
            else if(lastPosition.piecePosition.pieceIndex == position.piecePosition.pieceIndex)
            {
                accurateSpeed = (position.piecePosition.inPieceDistance - lastPosition.piecePosition.inPieceDistance) / (gameTick - lastTick);
            }

            dash.Update(this, accurateSpeed, gameTick);

            double currentGee = dash.Gee;
            double speed = dash.Speed;
            double angle = dash.LastAngle;

            maxAngleThroughCorner = Math.Max(maxAngleThroughCorner, Math.Abs(angle));
            maxSpeedThroughCorner = Math.Max(maxSpeedThroughCorner, speed);
            maxGeeThroughCorner = Math.Max(maxGeeThroughCorner, Math.Abs(currentGee));
            ticksThroughCorner++;
        }

        if (lookahead != null && myself)
        {
            lookahead.Update(this);
        }

        lastTick = gameTick;
    }

    public bool PositionProvided
    {
        get;
        set;
    }

    static int GetIntermediate(int index, int[] array)
    {
        int prev = index - 1;

        if(prev < 0)
        {
            prev = array.Length - 1;
        }

        if(array[prev] == int.MaxValue)
        {
            return array[index];
        }
        else
        {
            return array[index] - array[prev];
        }
    }

    public int CompareIntermediates(Player to, int count)
    {
        int ret = 0;
        int lastPiece = position.piecePosition.pieceIndex;

        for(int i = 0; i < count; i++)
        {
            int myIntermediate = GetIntermediate(lastPiece, intermediates);

            if(myIntermediate != int.MaxValue)
            {
                int toIntermediate = GetIntermediate(lastPiece, to.intermediates);

                if (toIntermediate != int.MaxValue)
                {
                    ret += (myIntermediate - toIntermediate);
                }
            }

            lastPiece = race.track.GetPrevIndex(lastPiece);
        }

        return ret;
    }

    public void ConsumeTurbo()
    {
        if(turboAvailable != null)
        {
            lastTurbo = turboAvailable;
            turboAvailable = null;
        }
    }

    public bool TurboAvailable()
    {
        if (turboReceived > 0)
        {
            return false;
        }

        return (turboAvailable != null);
    }

    public void AddTurbo(TurboData turbo)
    {
        turboAvailable = turbo;
        
        if(dash.Turbo == CarDash.TurboState.Turbo_OFF)
        {
            turboReceived = 2;
            dash.Turbo = CarDash.TurboState.Turbo_Available;
        }
    }

    public double GeeDefaultLimit()
    {
        return TrackProbe.DummyDefault;
    }

    public static double GeeFactor = 1.0;

    public double SpeedLimit(int index, int startLaneIndex, int endLaneIndex)
    {
        double speedLimit = 100000.0;
        TrackPart part = race.track.SafeAt(index);

        if (part.turn)
        {
            double geeLimit = TrackProbe.DummyDefault * dash.MeasuredSlipGee / 293;
            
            if (race.track.probe != null)
            {
                geeLimit = race.track.probe.GetGeeLimit(part.index);
            }

            geeLimit *= GeeFactor;
            geeLimit /= 1000.0;

            double radius = part.Radius(startLaneIndex, endLaneIndex);
            double circleDistance = 2.0 * Math.PI * radius;
            speedLimit = Math.Sqrt(geeLimit * circleDistance); //Kind of stupid...should be Math.Sqrt(geeForce * r)

            double safetyFactor = 1.0;

            if(!race.isQualifying)
            {
                if(raceState == RaceState.LeadingALot)
                {
                    safetyFactor = 0.99;
                }
                else if(raceState == RaceState.Leading)
                {
                    safetyFactor = 1.0;
                }
                else if(raceState == RaceState.Losing)
                {
                    safetyFactor = 1.0;
                }
                else
                {
                    safetyFactor = 1.01;
                }
            }

            speedLimit *= safetyFactor;
        }

        return speedLimit;
    }

    public void UpdateLeaderboard()
    {
        List<CarPositionMe> pos = new List<CarPositionMe>();

        foreach (var player in race.players.players.Values)
        {
            bool me = (player.Id == Id);
            pos.Add(new CarPositionMe(player.position, me));
        }

        pos.Sort(new Player_PosComparer());

        for(int i = 0; i < pos.Count; i++)
        {
            if(pos[i].Item2)
            {
                if(i == 0)
                {
                    raceState = RaceState.Leading;
                }
                else
                {
                    raceState = RaceState.Losing;
                }
            }
        }

        if(raceState == RaceState.Losing && pos.Count > 1)
        {
            if(pos[0].Item1.piecePosition.lap > position.piecePosition.lap)
            {
                raceState = RaceState.LosingALot;
            }
            else if(pos[0].Item1.piecePosition.pieceIndex > position.piecePosition.pieceIndex + 4)
            {
                raceState = RaceState.LosingALot;
            }
        }
        else if(raceState == RaceState.Leading && pos.Count > 1)
        {
            if (pos[1].Item1.piecePosition.lap < position.piecePosition.lap)
            {
                raceState = RaceState.LeadingALot;
            }
            else if (pos[1].Item1.piecePosition.pieceIndex + 8 < position.piecePosition.pieceIndex)
            {
                raceState = RaceState.LeadingALot;
            }
        }
    }

    public int[] intermediates;
    public double lastMoved;
    public FullRace race;
    public Car car;
    public CarPosition position;
    public CarDash dash;
    public LookAhead lookahead;
    public bool myself;
    private int lastTick;
    private string id;
    private bool turboState = false;
    private TurboData turboAvailable = null;
    private TurboData lastTurbo = null;
    private int turboReceived = 0;
    private bool crashOccurred = false;
    private RaceState raceState = RaceState.Losing;

    private double maxGeeThroughCorner = 0.0;
    private double maxAngleThroughCorner = 0.0;
    private double maxSpeedThroughCorner = 0.0;
    private int ticksThroughCorner = 0;
    private int measuringTicks = 0;
}

public class CarPositionMe
{
    public CarPosition Item1;
    public bool Item2;

    public CarPositionMe(CarPosition m, bool e)
    {
        Item1 = m;
        Item2 = e;
    }
}

public class Player_PosComparer : IComparer<CarPositionMe>
{
    public int Compare(CarPositionMe x, CarPositionMe y)
    {
        int ret = y.Item1.piecePosition.lap.CompareTo(x.Item1.piecePosition.lap);

        if(ret == 0)
        {
            ret = y.Item1.piecePosition.pieceIndex.CompareTo(x.Item1.piecePosition.pieceIndex);

            if(ret == 0)
            {
                ret = y.Item1.piecePosition.inPieceDistance.CompareTo(x.Item1.piecePosition.inPieceDistance);
            }
        }

        return ret;
    }
}

public class Players
{
    public void AddMe(CarId id)
    {
        if(me == null)
        {
            if(!players.TryGetValue(id.color, out me))
            {
                me = new Player();
                players.Add(id.color, me);
                me.Id = id.color;
            }

            me.myself = true;
        }
    }

    public void AddCars(Car[] cars)
    {
        foreach(var car in cars)
        {
            AddCar(car);
        }
    }

    public Player AddCar(Car car)
    {
        Player player;

        if (!players.TryGetValue(car.id.color, out player))
        {
            player = new Player(car);
            players.Add(car.id.color, player);
            player.Id = car.id.color;
        }
        else
        {
            player.SetCar(car);
        }

        return player;
    }

    public Player AddCar(CarId id)
    {
        Player player;

        if (!players.TryGetValue(id.color, out player))
        {
            player = new Player();
            players.Add(id.color, player);
            player.Id = id.color;
        }

        return player;
    }

    public void Init(FullRace race)
    {
        foreach(var player in players.Values)
        {
            player.Init(race);
        }
    }

    public void TurboAvailable(TurboData data)
    {
        foreach(var player in players.Values)
        {
            player.AddTurbo(data);
        }
    }

    public void Spawned(CrashSpawn data)
    {
        Player player;

        if(players.TryGetValue(data.color, out player))
        {
            player.Spawned();
        }
    }

    public void Crashed(CrashSpawn data)
    {
        Player player;

        if(players.TryGetValue(data.color, out player))
        {
            player.Crashed();
        }
    }

    public void NewPosition(CarPosition[] positions, int gameTick)
    {
        foreach (var player in players.Values)
        {
            player.PositionProvided = false;
        }

        foreach(var position in positions)
        {
            Player player;

            if (!players.TryGetValue(position.id.color, out player))
            {
                //HMM...internesting...new guy appeared...
                player = AddCar(position.id);
            }

            player.NewPosition(position, gameTick);
            player.PositionProvided = true;
        }


        /*
        foreach (var player in players.Values)
        {
            if(!player.myself)
            {
                //Should we removed all suckers without position
                //Can there be sudden appearance of slow fellows blocking the roads??
                if(!player.PositionProvided)
                {
                    players.Remove(player.Id);
                }
            }
        }
         */
    }

    public void TurboOn(TurboMessage data)
    {
        Player player;

        if (players.TryGetValue(data.color, out player))
        {
            player.TurboState = true;
        }
    }

    public void TurboOff(TurboMessage data)
    {
        Player player;

        if (players.TryGetValue(data.color, out player))
        {
            player.TurboState = false;
        }
    }

    public Player me;
    public Dictionary<string, Player> players = new Dictionary<string, Player>();
}

public class DriverHerring : Driver
{
    int lastSwitchPiece = -1;
    double lastSpeed = 0.0;

    // start small
    double measuredDvPerTick = 0.98;
    double previousV = -1.0;
    bool isBreaking = false;
    int lastPieceIndex = -1;

    static bool firstCurveMeasure = true;

    public override SendMsg Update(FullRace gRace, int gameTick)
    {
        SendMsg response = GetSwitchResponse(gRace);

        if(response == null)
        {
            Player me = gRace.players.me;
            TrackPart currentPiece = gRace.track.SafeAt(me.position.piecePosition.pieceIndex);

            //Dummy for collision testing...

            double throttleAmount = 1.0;

            if (me.dash.Speed != lastSpeed)
            {
                lastSpeed = me.dash.Speed;

                // we're on a straigth - brake for next turn
                double lookAheadthrottle = LookAhead(me.dash, me.lookahead, isBreaking);
                double curveThrottle = getOptimumCurveThrottle(me);
                throttleAmount = Math.Min(lookAheadthrottle, curveThrottle);

                if (throttleAmount == 0.0)
                {
                    isBreaking = true;
                    //response = new Throttle(0.0);
                    //myCarDash.SetThrottle(0.0);
                }
                else
                {
                    isBreaking = false;
                    //response = new Throttle(1.0);
                    //myCarDash.SetThrottle(1.0);
                }
            }

            if(me.position.piecePosition.lap == gRace.laps - 1 &&
                currentPiece.lastStraight)
            {
                throttleAmount = 1.0;
            }

            bool doTurbo = false;

            if(throttleAmount >= 0.33 && me.dash.Throttle >= 0.33f)
            {
                if (me.TurboAvailable() && currentPiece.index != lastPieceIndex)
                {
                    double straightLeft = currentPiece.straightLeft - me.position.piecePosition.inPieceDistance;

                    //TODO: DO SOME REAL CALCULATIONS ON IS IT SAFE?

                    if (currentPiece.turboPriority == 0 || currentPiece.turboPriority == 1)
                    {
                        doTurbo = true;
                    }

                    lastPieceIndex = currentPiece.index;
                }
            }

            if(doTurbo)
            {
                response = new Turbo("Go fast...");
                me.ConsumeTurbo();
            }
            else
            {
                response = new Throttle(throttleAmount);
                me.dash.Throttle = throttleAmount;
            }
        }

        return response;
    }

    bool decDone = false;

    private SendMsg GetSwitchResponse(FullRace gRace)
    {
        Player me = gRace.players.me;
        int decision = 0;

        TrackPart currentPiece = gRace.track.SafeAt(me.position.piecePosition.pieceIndex);
        TrackPart nextPiece = gRace.track.GetNextPart(currentPiece);

        if(decDone)
        {
            //Console.WriteLine(currentPiece.index.ToString() + " DEC NEXT");
            decDone = false;
        }

        if (nextPiece.switcher && nextPiece.index != lastSwitchPiece)
        {
            int currentLane = me.position.piecePosition.lane.endLaneIndex;
            double distLeft = currentPiece.Length(me.position.piecePosition.lane);
            distLeft -= me.position.piecePosition.inPieceDistance;
            distLeft -= (me.dash.Speed + me.dash.Acceleration) * 3; //Next...

            if (distLeft <= 0.0)
            {
                decDone = true;
                lastSwitchPiece = nextPiece.index;

                TrackPart nextSwitch = null;
                int searchIndex = gRace.track.GetNextIndex(nextPiece.index);

                while (nextSwitch == null && searchIndex != nextPiece.index)
                {
                    nextSwitch = gRace.track.SafeAt(searchIndex);

                    if(!nextSwitch.switcher)
                    {
                        nextSwitch = null;
                        searchIndex = gRace.track.GetNextIndex(searchIndex);
                    }
                }

                //There can be only one...
                if(nextSwitch == null)
                {
                    nextSwitch = nextPiece;
                }

                double lefts = 0, rights = 0, leftAngles = 0, rightAngles = 0, straights = 0;
                List<List<Player>> enemies = new List<List<Player>>();
                enemies.Add(new List<Player>());
                enemies.Add(new List<Player>());
                enemies.Add(new List<Player>());
                
                var iterPiece = nextPiece;
                bool first = true;

                do
                {
                    //First piece doesn't affect to lane decision...
                    if(!first)
                    {
                        if (iterPiece.angle < 0.0)
                        {
                            leftAngles += Math.Abs(iterPiece.angle);
                            lefts += iterPiece.Length();
                        }
                        else if (iterPiece.angle > 0.0)
                        {
                            rightAngles += iterPiece.angle;
                            rights += iterPiece.Length();
                        }
                        else
                        {
                            straights += iterPiece.Length();
                        }
                    }
                    else
                    {
                        first = false;
                    }

                    var lEnemies = iterPiece.EnemiesOnLane(iterPiece.GetLaneIndexAt(currentLane, -1), me);
                    var cEnemies = iterPiece.EnemiesOnLane(currentLane, me);
                    var rEnemies = iterPiece.EnemiesOnLane(iterPiece.GetLaneIndexAt(currentLane, 1), me);

                    if(lEnemies != null)
                    {
                        enemies[0].AddRange(lEnemies);
                    }

                    if (cEnemies != null)
                    {
                        enemies[1].AddRange(cEnemies);
                    }

                    if (rEnemies != null)
                    {
                        enemies[2].AddRange(rEnemies);
                    }

                    iterPiece = gRace.track.GetNextPart(iterPiece);
                }
                while (iterPiece.index != nextSwitch.index);

                if (lefts > rights)
                {
                    decision = -1;
                }
                else if (rights > lefts)
                {
                    decision = 1;
                }
                else if (rightAngles > leftAngles)
                {
                    decision = 1;
                }
                else if (leftAngles > rightAngles)
                {
                    decision = -1;
                }
                //Taking outside path from corner...
                else if (nextPiece.angle < 0.0)
                {
                    decision = 1;
                }
                else if (nextPiece.angle > 0.0)
                {
                    decision = -1;
                }
                else
                {
                    decision = 0;
                }

                int[] possibilities = new int[3] { 1000, 1000, 1000 };
                int posIndex = 0;

                if (nextPiece.GetLaneIndexAt(currentLane, decision) == -1)
                {
                    possibilities[posIndex++] = 0;
                }
                else
                {
                    possibilities[posIndex++] = decision;
                    possibilities[posIndex++] = 0;
                }

                if (nextPiece.GetLaneIndexAt(currentLane, -decision) != -1)
                {
                    possibilities[posIndex++] = -decision;
                }

                decision = CheckCollision(possibilities, enemies, gRace, nextSwitch);
            }

            if (decision > 0)
            {
                return SwitchLane.SwitchRight();
            }
            else if (decision < 0)
            {
                return SwitchLane.SwitchLeft();
            }
        }

        return null;
    }

    private int CheckCollision(int[] changes, List<List<Player>> enemies, FullRace gRace, TrackPart untilPart)
    {
        int decision = 0;

        for (int i = 0; i < changes.Length; i++ )
        {
            int chg = changes[i];

            if (chg != 1000)
            {
                List<Player> enemylist = enemies[chg + 1];

                if (enemylist.Count == 0)
                {
                    decision = chg;
                    break;
                }
                else
                {
                    bool goAround = false;

                    foreach(var enemy in enemylist)
                    {
                        //If it just sits there...we go around
                        if(enemy.dash.Speed == 0.0)
                        {
                            goAround = true;
                            break;
                        }
                        else
                        {
                            double fwdDistasnce = gRace.track.ForwardDistanceBetween(gRace.players.me, enemy);
                            int inComparison = gRace.players.me.CompareIntermediates(enemy, 5);

                            if(inComparison < 0 || (inComparison < 10 && fwdDistasnce < 100))
                            {
                                goAround = true;
                            }
                        }
                    }

                    if(!goAround)
                    {
                        decision = chg;
                        break;
                    }
                }
            }
        }

        return decision;
    }

    private double LookAhead(CarDash dash, LookAhead lookAhead, bool wasBraking)
    {
        var cSpeed = dash.Speed;
        var tSpeed = lookAhead.TargetSpeed;

        if (cSpeed < tSpeed)
        {
            return 1.0;
        }

        // simulate breaking ot get needed distance;
        //  dv /cSpeed = 0.02.. dv / tick  = 0.02 * cSpeed
        if (previousV > 0 && wasBraking)
        {
            //current speed should be lower but it's not guaranteed right after throttle -> brake change!
            if (cSpeed < previousV)
            {
                // decelarting
                measuredDvPerTick = 1 - ((previousV - cSpeed) / previousV);
                dash.MeasuredBrakeDecelaration = 1 - measuredDvPerTick;
                //Console.WriteLine("measuredBrakeDecel: " + dash.MeasuredBrakeDecelaration);
            }
            else
            {
                //accelerating??
                double candidate = 1 - ((cSpeed - previousV) / cSpeed);
                if (candidate < 1)
                {
                    measuredDvPerTick = candidate;
                }
                //Console.WriteLine("speeding: " + measuredDvPerTick);
            }
        }
        var simSpeed = cSpeed;
        int tickCount = 0;
        double simdistTraveled = 0;

        while (simSpeed >= tSpeed)
        {
            //tested dDV = 0.98 on full braking on keimola track
            // this autocalibrates to current conditions :)
            simSpeed = measuredDvPerTick * simSpeed;
            simdistTraveled += simSpeed;
            tickCount++;
        }
        previousV = cSpeed;
        var neededBreakDistance = simdistTraveled;
        //Console.WriteLine("neededBreakDistance " + neededBreakDistance);
        //Console.WriteLine("measuredDvPerTick " + measuredDvPerTick);
        //Console.WriteLine("lookAhead.Distance " + lookAhead.Distance);
        if (neededBreakDistance < 0)
        {
            return 1.0;
            //send(new Throttle(1.0));
        }
        else
        {
            if (neededBreakDistance < lookAhead.Distance)
            {
                return 1.0;
            }
            else
            {
                return stabilizeToSpeed(dash, cSpeed, lookAhead.TargetSpeed);
            }
        }
    }
    double initialCurveProbeSpeed = 4.0;
    double currentCurveProbeThrottle = 0.4;
    double currentCurveProbeSpeed;
    double maxNonSlipThrottle;
    double maxNonSlipGee;
    private double getOptimumCurveThrottle(Player player)
    {
        double throttle;
        var speedLimit = player.SpeedLimit(player.position.piecePosition.pieceIndex,
                                            player.position.piecePosition.lane.startLaneIndex, 
                                            player.position.piecePosition.lane.endLaneIndex);

        int ticksUntilStraight = 0;
        double cornerDistance = 0.0;

        TrackPart current = player.race.track.parts[player.position.piecePosition.pieceIndex];

        // if we haven't done a curve yet, go slow.
        // todo: this needs to happen in qualifying only, in order to measure track slippiness
        if (firstCurveMeasure && !current.turn && player.dash.LastAngle == 0.0)
        {
            return stabilizeToSpeed(player.dash, player.dash.Speed, initialCurveProbeSpeed);
        }
        else if (firstCurveMeasure && current.turn)
        {
            if (player.dash.LastAngle == 0.0)
            {
                currentCurveProbeThrottle += 0.01;
                initialCurveProbeSpeed = player.dash.Speed;
                return currentCurveProbeThrottle;
            }
            else if (player.dash.LastAngle > 0.0)
            {
                // we've hit the probing speed.
                // stop measuring and record the known values
                maxNonSlipThrottle = currentCurveProbeThrottle - 0.01;
                Console.WriteLine("maxNonSlipThrottle: " + maxNonSlipThrottle);
                Console.WriteLine("maxNonSlipSpeed: " + player.dash.Speed);
                Console.WriteLine("maxNonSlipGee: " + player.dash.Gee);
                // this is the geeforce that causes slipping. In normal conditions: 293
                maxNonSlipGee = player.dash.Gee;
                // compensate 0.98 for measurement errors
                player.dash.MeasuredSlipGee = Math.Abs(maxNonSlipGee*0.95);
                firstCurveMeasure = false;
            }
        }
        if(current.turn)
        {
            cornerDistance += current.Length(player.position.piecePosition.lane) - player.position.piecePosition.inPieceDistance;
            current = player.race.track.GetNextPart(current);

            while(current.turn)
            {
                cornerDistance += current.Length(player.position.piecePosition.lane.endLaneIndex, player.position.piecePosition.lane.endLaneIndex);
                current = player.race.track.GetNextPart(current);
            }

            if(cornerDistance > 0.0)
            {
                ticksUntilStraight = (int)Math.Ceiling(cornerDistance / player.dash.Speed);
            }
        }

        double simAV = player.dash.AngularVelocity;
        double simAA = player.dash.AngularAcceleration;
        double simAngle = player.dash.LastAngle;

        for (int i = 0; i < ticksUntilStraight; i++)
        {
            simAngle += simAV;
            simAV += simAA;
            if (Math.Abs(simAngle) >= 60)
            {
                if(i < 10)
                {
                    return 0.0;
                }
            }
        }

        var currentSpeed = player.dash.Speed;
        throttle = stabilizeToSpeed(player.dash, currentSpeed, speedLimit);

        //Console.WriteLine("throttle: " + throttle);
        return throttle;
    }

    private static double stabilizeToSpeed(CarDash myCarDash, double currentSpeed, double speedLimit)
    {
        double throttle = 0;
        if (speedLimit > myCarDash.CarPower) //10 changed to myCarDash.CarPower?
        {
            return 1.0;
        }
        var speedDiff = speedLimit - currentSpeed;
        var ratio = (speedLimit - speedDiff) / speedLimit;
        // were under speed limit
        if (speedDiff > 0)
        {
            if (ratio > 0.7)
            {
                throttle = Math.Min(1.0, speedLimit / myCarDash.CarPower + (1 - ratio) * (speedLimit)); //10 changed to myCarDash.CarPower
            }
            else
            {
                throttle = 1.0;
            }

        }
        // we're over speed limit
        else
        {
            if (Math.Abs(ratio) < 0.3)
            {

                //Console.WriteLine("braking, ratio: " + ratio);
                //Console.WriteLine("speeddiff: " + speedDiff);
                //Console.WriteLine("currentspeed: " + currentSpeed);

                if (speedDiff == 0)
                {
                    throttle = (speedLimit / myCarDash.CarPower); //10 changed to myCarDash.CarPower
                }
                else
                {
                    throttle = Math.Max(0, speedLimit / myCarDash.CarPower - (ratio) * (speedLimit)); //10 changed to myCarDash.CarPower
                }
            }
            else
            {
                throttle = 0.0;
            }
        }
        return throttle;
    }
}

public class TrackPartLimits
{
    public TrackPartLimits()
    {
        maxGee = 0;
    }

    public double maxGee;
}

public class TrackProbe
{
    public const double DummyDefault = 81;

    public TrackProbe()
    {
        
    }

    public TrackProbe(string name, FullTrack trk)
    {
        this.name = name;
        pieces = new TrackPartLimits[trk.parts.Count];
        defaultLimit = DummyDefault;
    }

    public static TrackProbe Create(string name, FullTrack trk)
    {
        TrackProbe ret = null;

        try
        {
            using (StreamReader sr = new StreamReader(name))
            {
                String line = sr.ReadToEnd();
                ret = JsonConvert.DeserializeObject<TrackProbe>(line);
            }
        }
        catch(Exception)
        {
            
        }

        if(ret == null)
        {
            ret = new TrackProbe(name, trk);
        }

        return ret;
    }

    public void UpgradeMaxGee(int index, double value)
    {
        if (pieces != null && index >= 0 && index < pieces.Length)
        {
            if(pieces[index] == null)
            {
                pieces[index] = new TrackPartLimits();
                pieces[index].maxGee = defaultLimit;
            }

            pieces[index].maxGee += value;
        }
    }

    public static void Save(TrackProbe probe)
    {
        try
        {
            using (StreamWriter sr = new StreamWriter(probe.name, false))
            {
                String data = JsonConvert.SerializeObject(probe);
                sr.Write(data);
            }
        }
        catch (Exception)
        {

        }
    }

    public double GetGeeLimit(int index)
    {
        double ret = defaultLimit;

        if(pieces != null && index >= 0 && index < pieces.Length && pieces[index] != null)
        {
            ret = pieces[index].maxGee;

            if(ret == 0.0)
            {
                ret = defaultLimit;
            }
        }

        return ret;
    }

    public string name;
    public TrackPartLimits[] pieces;
    public double defaultLimit = DummyDefault;
}