﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Herring
{
    public partial class TrackView : UserControl
    {
        Track mTrack;
        Dictionary<string, CarPosition> carPos = new Dictionary<string, CarPosition>();

        public TrackView()
        {
            InitializeComponent();
        }

        delegate void SetTrackDelegate(Track trak);

        public void SetTrack(Track trak)
        {
            if (InvokeRequired)
            {
                Invoke(new SetTrackDelegate(SetTrack), trak);
            }
            else
            {
                mTrack = trak;
                Invalidate();
            }
        }

        public void BotPosition(CarId id, CarPosition pos)
        {
            if(pos != null)
            {

            
            carPos.Remove(id.color);
            carPos.Add(id.color, pos);
            
            Invalidate();
            }
        }

        private void TrackView_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.LightGray);
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            if(mTrack != null)
            {
                GraphicsPath path = new GraphicsPath();
                double direction = 0.0f;
                PointF currentPoint = new PointF(0.0f, 0.0f);
                int index = 0;

                List<PointF> points = new List<PointF>();
                List<Color> colors = new List<Color>();

                foreach (var piece in mTrack.pieces)
                {
                    if(piece.length != 0.0)
                    {
                        PointF endPoint = new PointF(currentPoint.X, currentPoint.Y);
                        endPoint.X += (float)(Math.Cos(direction) * piece.length);
                        endPoint.Y += (float)(Math.Sin(direction) * piece.length);
                        path.AddLine(currentPoint, endPoint);

                        foreach(var item in carPos)
                        {
                            if(item.Value.piecePosition.pieceIndex == index)
                            {
                                PointF pt = new PointF(currentPoint.X, currentPoint.Y);
                                pt.X += (float)(Math.Cos(direction) * item.Value.piecePosition.inPieceDistance);
                                pt.Y += (float)(Math.Sin(direction) * item.Value.piecePosition.inPieceDistance);

                                points.Add(pt);
                                colors.Add(Color.FromName(item.Key));
                            }
                        }

                        currentPoint = endPoint;
                    }
                    else
                    {
                        double angle = piece.angle / 180.0 * Math.PI;
                        double radius = piece.radius;
                        double nextDir = direction + angle;

                        double length = (2.0 * Math.PI * radius) * (Math.Abs(angle) / (Math.PI * 2.0));

                        PointF endPoint = new PointF(currentPoint.X, currentPoint.Y);
                        endPoint.X += (float)(Math.Cos(nextDir) * length);
                        endPoint.Y += (float)(Math.Sin(nextDir) * length);

                        foreach (var item in carPos)
                        {
                            if (item.Value.piecePosition.pieceIndex == index)
                            {
                                PointF pt = new PointF(currentPoint.X, currentPoint.Y);
                                pt.X += (float)(Math.Cos(nextDir) * item.Value.piecePosition.inPieceDistance);
                                pt.Y += (float)(Math.Sin(nextDir) * item.Value.piecePosition.inPieceDistance);

                                points.Add(pt);
                                colors.Add(Color.FromName(item.Key));
                            }
                        }

                        direction = nextDir;
                        path.AddLine(currentPoint, endPoint);
                        currentPoint = endPoint;
                    }

                    ++index;
                }

                var bounds = path.GetBounds();
                float xScale = (float)e.ClipRectangle.Width / bounds.Width;
                float yScale = (float)e.ClipRectangle.Height / bounds.Height;
                xScale = Math.Min(xScale, yScale);

                e.Graphics.TranslateTransform(-bounds.X + 20, -bounds.Y + 20);
                e.Graphics.ScaleTransform(xScale * 0.9f, xScale * 0.9f, MatrixOrder.Append);
                e.Graphics.DrawPath(new Pen(new SolidBrush(Color.Black), 3.0f / xScale), path);

                float size = 20.0f / xScale;

                for(int i = 0; i < points.Count; i++)
                {
                    e.Graphics.FillEllipse(new SolidBrush(colors[i]), new RectangleF(points[i].X - size / 2.0f,
                                                                                    points[i].Y - size / 2.0f,
                                                                                    size, size));
                }
            }
        }

        private void TrackView_SizeChanged(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
