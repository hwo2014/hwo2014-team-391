﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Herring
{
    public partial class CarAngleView : UserControl
    {
        double _angle = 0.0;
        Color carColor = Color.Beige;
        float carWidth = 20.0f;
        float carLength = 40.0f;
        float ohjuri = 10.0f;

        public CarAngleView()
        {
            InitializeComponent();
        }

        public void SetAngle(double angle)
        {
            if(_angle != angle)
            {
                _angle = angle;
                Invalidate();
            }
        }

        public void SetCar(Car car)
        {
            if(car != null)
            {
                Color newClr = Color.FromName(car.id.color);
                float newW = carWidth;
                float newL = carLength;
                float newO = ohjuri;

                if(car.dimensions != null)
                {
                    newW = (float)car.dimensions.width;
                    newL = (float)car.dimensions.length;
                    newO = (float)car.dimensions.guideFlagPosition;
                }

                if(newClr != carColor || newW != carWidth || newL != carLength || newO != ohjuri)
                {
                    carColor = newClr;
                    carWidth = newW;
                    carLength = newL;
                    ohjuri = newO;
                    Invalidate();
                }
            }
        }

        private void CarAngleView_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.LightGray);
            e.Graphics.DrawLine(new Pen(new SolidBrush(Color.Black)),
                                new Point(e.ClipRectangle.Left + e.ClipRectangle.Width / 2, e.ClipRectangle.Top),
                                new Point(e.ClipRectangle.Left + e.ClipRectangle.Width / 2, e.ClipRectangle.Bottom));

            e.Graphics.DrawLine(new Pen(new SolidBrush(Color.Black)),
                                new Point(e.ClipRectangle.Left, e.ClipRectangle.Top + e.ClipRectangle.Height / 2),
                                new Point(e.ClipRectangle.Right, e.ClipRectangle.Top + e.ClipRectangle.Height / 2));

            float width = e.ClipRectangle.Width / 4;
            float multiplier = width / carWidth;
            float height = multiplier * carLength;
            float guideFlag = multiplier * ohjuri;

            float guideOffset = carLength / 2.0f - guideFlag;

            e.Graphics.TranslateTransform(0.0f, guideOffset, System.Drawing.Drawing2D.MatrixOrder.Prepend);
            e.Graphics.RotateTransform((float)_angle, System.Drawing.Drawing2D.MatrixOrder.Append);
            e.Graphics.TranslateTransform((float)e.ClipRectangle.Width / 2, (float)e.ClipRectangle.Height / 2, System.Drawing.Drawing2D.MatrixOrder.Append);
            
            
            
            e.Graphics.FillRectangle(new SolidBrush(carColor),
                                    new RectangleF(-width/2, -height / 2, width, height));

            e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)),
                                    -width / 2, -height / 2, width, height);

        }
    }
}
