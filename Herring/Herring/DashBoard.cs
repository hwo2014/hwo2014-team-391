﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Herring
{
    public partial class DashBoard : UserControl
    {


        public DashBoard()
        {
            InitializeComponent();
        }

        public void View(PlotData data)
        {
            if (data.Dashboard != null)
            {
                curSpeed.Text = string.Format("{0:0.000}", data.Dashboard.Speed);
                speedoMeter.Value = (int)Math.Max(0.0, Math.Min(data.Dashboard.Speed * 10, 100));

                double geed = data.Dashboard.Gee;
                geed = Math.Max(-20, Math.Min(geed, 20));
                geed *= 50;
                int geeInt = (int)geed;

                if(geeInt < 0)
                {
                    geeLeft.Value = -geeInt;
                    geeRight.Value = 0;
                }
                else
                {
                    geeRight.Value = geeInt;
                    geeLeft.Value = 0;
                }

                curretG.Text = string.Format("{0:00.000}", data.Dashboard.Gee);
                maxG.Text = string.Format("{0:00.000}", data.Dashboard.GeeMax);
                minG.Text = string.Format("{0:00.000}", data.Dashboard.GeeMin);

                angleView.SetAngle(data.Dashboard.LastAngle);
                AngleVal.Text = string.Format("{0:00.000}", data.Dashboard.LastAngle);

                throttle.Text = string.Format("{0:0.000}", data.Dashboard.Throttle);
                acceleration.Text = string.Format("{0:00.000}", data.Dashboard.Acceleration);
                angular.Text = string.Format("{0:00.000}", data.Dashboard.AngularVelocity);

                angularAcceleration.Text = string.Format("{0:00.000}", data.Dashboard.AngularAcceleration);
                inertia.Text = string.Format("{0:00.000}", data.Dashboard.CarInertia);
                torque.Text = string.Format("{0:00.000}", data.Dashboard.CarTorque);

                urpoState.Text = data.Dashboard.Turbo.ToString();

                if(data.Dashboard.Alive)
                {
                    alive.Text = "Alive";
                }
                else
                {
                    alive.Text = "DEAD!";
                }
            }

            if(data.ThisCar != null)
            {
                angleView.SetCar(data.ThisCar);
            }
            
        }

        public void Clear()
        {
            curSpeed.Text = "";
            speedoMeter.Value = 0;

            angleView.SetAngle(0.0);
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
    }
}
