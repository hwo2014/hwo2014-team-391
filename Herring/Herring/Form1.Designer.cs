﻿namespace Herring
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.germanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.franceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eläintarhaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.japanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createRaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joinRaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.playerTab = new System.Windows.Forms.TabControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.testServer = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.botEngCombo = new System.Windows.Forms.ToolStripComboBox();
            this.trackView = new Herring.TrackView();
            this.lookAheadControl1 = new Herring.LookAheadControl();
            this.label7 = new System.Windows.Forms.Label();
            this.pegPos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.carWidth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.carLength = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.germanyToolStripMenuItem,
            this.usaToolStripMenuItem,
            this.franceToolStripMenuItem,
            this.eläintarhaToolStripMenuItem,
            this.italyToolStripMenuItem,
            this.englandToolStripMenuItem,
            this.japanToolStripMenuItem,
            this.cancelToolStripMenuItem,
            this.advancedToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(864, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.startToolStripMenuItem.Text = "&Keimola";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // germanyToolStripMenuItem
            // 
            this.germanyToolStripMenuItem.Name = "germanyToolStripMenuItem";
            this.germanyToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.germanyToolStripMenuItem.Text = "&Germany";
            this.germanyToolStripMenuItem.Click += new System.EventHandler(this.germanyToolStripMenuItem_Click);
            // 
            // usaToolStripMenuItem
            // 
            this.usaToolStripMenuItem.Name = "usaToolStripMenuItem";
            this.usaToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.usaToolStripMenuItem.Text = "Usa";
            this.usaToolStripMenuItem.Click += new System.EventHandler(this.usaToolStripMenuItem_Click);
            // 
            // franceToolStripMenuItem
            // 
            this.franceToolStripMenuItem.Name = "franceToolStripMenuItem";
            this.franceToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.franceToolStripMenuItem.Text = "France";
            this.franceToolStripMenuItem.Click += new System.EventHandler(this.franceToolStripMenuItem_Click);
            // 
            // eläintarhaToolStripMenuItem
            // 
            this.eläintarhaToolStripMenuItem.Name = "eläintarhaToolStripMenuItem";
            this.eläintarhaToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.eläintarhaToolStripMenuItem.Text = "Eläintarha";
            this.eläintarhaToolStripMenuItem.Click += new System.EventHandler(this.eläintarhaToolStripMenuItem_Click);
            // 
            // italyToolStripMenuItem
            // 
            this.italyToolStripMenuItem.Name = "italyToolStripMenuItem";
            this.italyToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.italyToolStripMenuItem.Text = "Italy";
            this.italyToolStripMenuItem.Click += new System.EventHandler(this.italyToolStripMenuItem_Click);
            // 
            // englandToolStripMenuItem
            // 
            this.englandToolStripMenuItem.Name = "englandToolStripMenuItem";
            this.englandToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.englandToolStripMenuItem.Text = "England";
            this.englandToolStripMenuItem.Click += new System.EventHandler(this.englandToolStripMenuItem_Click);
            // 
            // japanToolStripMenuItem
            // 
            this.japanToolStripMenuItem.Name = "japanToolStripMenuItem";
            this.japanToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.japanToolStripMenuItem.Text = "Japan";
            this.japanToolStripMenuItem.Click += new System.EventHandler(this.japanToolStripMenuItem_Click);
            // 
            // cancelToolStripMenuItem
            // 
            this.cancelToolStripMenuItem.Enabled = false;
            this.cancelToolStripMenuItem.Name = "cancelToolStripMenuItem";
            this.cancelToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.cancelToolStripMenuItem.Text = "&Cancel";
            this.cancelToolStripMenuItem.Click += new System.EventHandler(this.cancelToolStripMenuItem_Click);
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createRaceToolStripMenuItem,
            this.joinRaceToolStripMenuItem});
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.advancedToolStripMenuItem.Text = "Advanced";
            // 
            // createRaceToolStripMenuItem
            // 
            this.createRaceToolStripMenuItem.Name = "createRaceToolStripMenuItem";
            this.createRaceToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.createRaceToolStripMenuItem.Text = "&Create Race";
            this.createRaceToolStripMenuItem.Click += new System.EventHandler(this.createRaceToolStripMenuItem_Click);
            // 
            // joinRaceToolStripMenuItem
            // 
            this.joinRaceToolStripMenuItem.Name = "joinRaceToolStripMenuItem";
            this.joinRaceToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.joinRaceToolStripMenuItem.Text = "&Join Race";
            this.joinRaceToolStripMenuItem.Click += new System.EventHandler(this.joinRaceToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 592);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(864, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // playerTab
            // 
            this.playerTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerTab.Location = new System.Drawing.Point(12, 276);
            this.playerTab.Name = "playerTab";
            this.playerTab.SelectedIndex = 0;
            this.playerTab.Size = new System.Drawing.Size(840, 313);
            this.playerTab.TabIndex = 3;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.testServer,
            this.toolStripLabel2,
            this.botEngCombo});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(864, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(66, 22);
            this.toolStripLabel1.Text = "Test server:";
            // 
            // testServer
            // 
            this.testServer.Items.AddRange(new object[] {
            "testserver.helloworldopen.com",
            "hakkinen.helloworldopen.com",
            "senna.helloworldopen.com",
            "webber.helloworldopen.com",
            "prost.helloworldopen.com"});
            this.testServer.Name = "testServer";
            this.testServer.Size = new System.Drawing.Size(220, 25);
            this.testServer.SelectedIndexChanged += new System.EventHandler(this.testServer_SelectedIndexChanged_1);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(64, 22);
            this.toolStripLabel2.Text = "BotEngine:";
            // 
            // botEngCombo
            // 
            this.botEngCombo.Name = "botEngCombo";
            this.botEngCombo.Size = new System.Drawing.Size(200, 25);
            // 
            // trackView
            // 
            this.trackView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackView.Location = new System.Drawing.Point(466, 52);
            this.trackView.Margin = new System.Windows.Forms.Padding(4);
            this.trackView.Name = "trackView";
            this.trackView.Size = new System.Drawing.Size(386, 218);
            this.trackView.TabIndex = 5;
            // 
            // lookAheadControl1
            // 
            this.lookAheadControl1.Location = new System.Drawing.Point(12, 52);
            this.lookAheadControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lookAheadControl1.Name = "lookAheadControl1";
            this.lookAheadControl1.Size = new System.Drawing.Size(310, 218);
            this.lookAheadControl1.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(356, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Peg:";
            // 
            // pegPos
            // 
            this.pegPos.Enabled = false;
            this.pegPos.Location = new System.Drawing.Point(359, 183);
            this.pegPos.Multiline = true;
            this.pegPos.Name = "pegPos";
            this.pegPos.ReadOnly = true;
            this.pegPos.Size = new System.Drawing.Size(100, 23);
            this.pegPos.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(356, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "CarWidth:";
            // 
            // carWidth
            // 
            this.carWidth.Enabled = false;
            this.carWidth.Location = new System.Drawing.Point(359, 132);
            this.carWidth.Multiline = true;
            this.carWidth.Name = "carWidth";
            this.carWidth.ReadOnly = true;
            this.carWidth.Size = new System.Drawing.Size(100, 23);
            this.carWidth.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(356, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "CarLength:";
            // 
            // carLength
            // 
            this.carLength.Enabled = false;
            this.carLength.Location = new System.Drawing.Point(359, 77);
            this.carLength.Multiline = true;
            this.carLength.Name = "carLength";
            this.carLength.ReadOnly = true;
            this.carLength.Size = new System.Drawing.Size(100, 23);
            this.carLength.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 614);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pegPos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.carWidth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.carLength);
            this.Controls.Add(this.lookAheadControl1);
            this.Controls.Add(this.trackView);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.playerTab);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Herring Studio 2014";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem cancelToolStripMenuItem;
        private System.Windows.Forms.TabControl playerTab;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox testServer;
        private System.Windows.Forms.ToolStripMenuItem germanyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createRaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joinRaceToolStripMenuItem;
        private TrackView trackView;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox botEngCombo;
        private System.Windows.Forms.ToolStripMenuItem usaToolStripMenuItem;
        private LookAheadControl lookAheadControl1;
        private System.Windows.Forms.ToolStripMenuItem franceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eläintarhaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem japanToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox pegPos;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox carWidth;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox carLength;
    }
}

