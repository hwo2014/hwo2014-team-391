﻿namespace Herring
{
    partial class DashBoard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.speedoMeter = new System.Windows.Forms.ProgressBar();
            this.curSpeed = new System.Windows.Forms.TextBox();
            this.maxSpeed = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.geeLeft = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.maxG = new System.Windows.Forms.TextBox();
            this.curretG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.minG = new System.Windows.Forms.TextBox();
            this.geeRight = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.throttle = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.acceleration = new System.Windows.Forms.TextBox();
            this.alive = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.angular = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.AngleVal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lapTime = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.angularAcceleration = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.inertia = new System.Windows.Forms.TextBox();
            this.angleView = new Herring.CarAngleView();
            this.label13 = new System.Windows.Forms.Label();
            this.torque = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.urpoState = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // speedoMeter
            // 
            this.speedoMeter.ForeColor = System.Drawing.Color.Blue;
            this.speedoMeter.Location = new System.Drawing.Point(21, 22);
            this.speedoMeter.MarqueeAnimationSpeed = 0;
            this.speedoMeter.Name = "speedoMeter";
            this.speedoMeter.Size = new System.Drawing.Size(200, 23);
            this.speedoMeter.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.speedoMeter.TabIndex = 0;
            // 
            // curSpeed
            // 
            this.curSpeed.Enabled = false;
            this.curSpeed.Location = new System.Drawing.Point(227, 22);
            this.curSpeed.Multiline = true;
            this.curSpeed.Name = "curSpeed";
            this.curSpeed.ReadOnly = true;
            this.curSpeed.Size = new System.Drawing.Size(100, 23);
            this.curSpeed.TabIndex = 1;
            this.curSpeed.Text = "Palala";
            // 
            // maxSpeed
            // 
            this.maxSpeed.Enabled = false;
            this.maxSpeed.Location = new System.Drawing.Point(333, 22);
            this.maxSpeed.Multiline = true;
            this.maxSpeed.Name = "maxSpeed";
            this.maxSpeed.ReadOnly = true;
            this.maxSpeed.Size = new System.Drawing.Size(100, 23);
            this.maxSpeed.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(224, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Speed:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(330, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Max Speed:";
            // 
            // geeLeft
            // 
            this.geeLeft.ForeColor = System.Drawing.Color.Blue;
            this.geeLeft.Location = new System.Drawing.Point(21, 77);
            this.geeLeft.MarqueeAnimationSpeed = 0;
            this.geeLeft.Maximum = 1000;
            this.geeLeft.Name = "geeLeft";
            this.geeLeft.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.geeLeft.RightToLeftLayout = true;
            this.geeLeft.Size = new System.Drawing.Size(99, 23);
            this.geeLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.geeLeft.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(330, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Max G:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(224, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "G:";
            // 
            // maxG
            // 
            this.maxG.Enabled = false;
            this.maxG.Location = new System.Drawing.Point(333, 77);
            this.maxG.Multiline = true;
            this.maxG.Name = "maxG";
            this.maxG.ReadOnly = true;
            this.maxG.Size = new System.Drawing.Size(100, 23);
            this.maxG.TabIndex = 7;
            // 
            // curretG
            // 
            this.curretG.Enabled = false;
            this.curretG.Location = new System.Drawing.Point(227, 77);
            this.curretG.Multiline = true;
            this.curretG.Name = "curretG";
            this.curretG.ReadOnly = true;
            this.curretG.Size = new System.Drawing.Size(100, 23);
            this.curretG.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(436, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Min G:";
            // 
            // minG
            // 
            this.minG.Enabled = false;
            this.minG.Location = new System.Drawing.Point(439, 77);
            this.minG.Multiline = true;
            this.minG.Name = "minG";
            this.minG.ReadOnly = true;
            this.minG.Size = new System.Drawing.Size(100, 23);
            this.minG.TabIndex = 10;
            // 
            // geeRight
            // 
            this.geeRight.ForeColor = System.Drawing.Color.Blue;
            this.geeRight.Location = new System.Drawing.Point(122, 77);
            this.geeRight.MarqueeAnimationSpeed = 0;
            this.geeRight.Maximum = 1000;
            this.geeRight.Name = "geeRight";
            this.geeRight.Size = new System.Drawing.Size(99, 23);
            this.geeRight.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.geeRight.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(224, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Throttle:";
            // 
            // throttle
            // 
            this.throttle.Enabled = false;
            this.throttle.Location = new System.Drawing.Point(227, 128);
            this.throttle.Multiline = true;
            this.throttle.Name = "throttle";
            this.throttle.ReadOnly = true;
            this.throttle.Size = new System.Drawing.Size(100, 23);
            this.throttle.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(330, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Acceleration:";
            // 
            // acceleration
            // 
            this.acceleration.Enabled = false;
            this.acceleration.Location = new System.Drawing.Point(333, 128);
            this.acceleration.Multiline = true;
            this.acceleration.Name = "acceleration";
            this.acceleration.ReadOnly = true;
            this.acceleration.Size = new System.Drawing.Size(100, 23);
            this.acceleration.TabIndex = 16;
            // 
            // alive
            // 
            this.alive.Enabled = false;
            this.alive.Location = new System.Drawing.Point(439, 128);
            this.alive.Multiline = true;
            this.alive.Name = "alive";
            this.alive.ReadOnly = true;
            this.alive.Size = new System.Drawing.Size(100, 23);
            this.alive.TabIndex = 18;
            this.alive.Text = "Alive";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(436, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Angular Velocity:";
            // 
            // angular
            // 
            this.angular.Enabled = false;
            this.angular.Location = new System.Drawing.Point(439, 22);
            this.angular.Multiline = true;
            this.angular.Name = "angular";
            this.angular.ReadOnly = true;
            this.angular.Size = new System.Drawing.Size(100, 23);
            this.angular.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(123, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Angle:";
            // 
            // AngleVal
            // 
            this.AngleVal.Enabled = false;
            this.AngleVal.Location = new System.Drawing.Point(126, 128);
            this.AngleVal.Multiline = true;
            this.AngleVal.Name = "AngleVal";
            this.AngleVal.ReadOnly = true;
            this.AngleVal.Size = new System.Drawing.Size(95, 23);
            this.AngleVal.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(123, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Best lap:";
            // 
            // lapTime
            // 
            this.lapTime.Enabled = false;
            this.lapTime.Location = new System.Drawing.Point(126, 234);
            this.lapTime.Multiline = true;
            this.lapTime.Name = "lapTime";
            this.lapTime.ReadOnly = true;
            this.lapTime.Size = new System.Drawing.Size(95, 23);
            this.lapTime.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(127, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Angular acceleration:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // angularAcceleration
            // 
            this.angularAcceleration.Enabled = false;
            this.angularAcceleration.Location = new System.Drawing.Point(128, 179);
            this.angularAcceleration.Multiline = true;
            this.angularAcceleration.Name = "angularAcceleration";
            this.angularAcceleration.ReadOnly = true;
            this.angularAcceleration.Size = new System.Drawing.Size(95, 23);
            this.angularAcceleration.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(225, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Inertia:";
            // 
            // inertia
            // 
            this.inertia.Enabled = false;
            this.inertia.Location = new System.Drawing.Point(227, 179);
            this.inertia.Multiline = true;
            this.inertia.Name = "inertia";
            this.inertia.ReadOnly = true;
            this.inertia.Size = new System.Drawing.Size(95, 23);
            this.inertia.TabIndex = 28;
            // 
            // angleView
            // 
            this.angleView.Location = new System.Drawing.Point(21, 128);
            this.angleView.Margin = new System.Windows.Forms.Padding(4);
            this.angleView.Name = "angleView";
            this.angleView.Size = new System.Drawing.Size(99, 129);
            this.angleView.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(331, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Torque:";
            // 
            // torque
            // 
            this.torque.Enabled = false;
            this.torque.Location = new System.Drawing.Point(332, 179);
            this.torque.Multiline = true;
            this.torque.Name = "torque";
            this.torque.ReadOnly = true;
            this.torque.Size = new System.Drawing.Size(95, 23);
            this.torque.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(229, 218);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Urpostate:";
            // 
            // urpoState
            // 
            this.urpoState.Enabled = false;
            this.urpoState.Location = new System.Drawing.Point(232, 234);
            this.urpoState.Multiline = true;
            this.urpoState.Name = "urpoState";
            this.urpoState.ReadOnly = true;
            this.urpoState.Size = new System.Drawing.Size(95, 23);
            this.urpoState.TabIndex = 31;
            // 
            // DashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.urpoState);
            this.Controls.Add(this.torque);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.inertia);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.angularAcceleration);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lapTime);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AngleVal);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.angular);
            this.Controls.Add(this.alive);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.acceleration);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.throttle);
            this.Controls.Add(this.angleView);
            this.Controls.Add(this.geeRight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.minG);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maxG);
            this.Controls.Add(this.curretG);
            this.Controls.Add(this.geeLeft);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maxSpeed);
            this.Controls.Add(this.curSpeed);
            this.Controls.Add(this.speedoMeter);
            this.Name = "DashBoard";
            this.Size = new System.Drawing.Size(631, 277);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ProgressBar speedoMeter;
        public System.Windows.Forms.TextBox curSpeed;
        public System.Windows.Forms.TextBox maxSpeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ProgressBar geeLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox maxG;
        public System.Windows.Forms.TextBox curretG;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox minG;
        public System.Windows.Forms.ProgressBar geeRight;
        private CarAngleView angleView;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox throttle;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox acceleration;
        public System.Windows.Forms.TextBox alive;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox angular;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox AngleVal;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox lapTime;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox angularAcceleration;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox inertia;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox torque;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox urpoState;
    }
}
