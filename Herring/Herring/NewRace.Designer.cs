﻿namespace Herring
{
    partial class NewRace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.track = new System.Windows.Forms.ComboBox();
            this.botsLabel = new System.Windows.Forms.Label();
            this.botbar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.botName = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.botbar)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(15, 262);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 25;
            this.okButton.Text = "&OK";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(96, 262);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 26;
            this.cancelButton.Text = "&Cancel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Track:";
            // 
            // track
            // 
            this.track.FormattingEnabled = true;
            this.track.Items.AddRange(new object[] {
            "keimola",
            "germany",
            "usa",
            "france"});
            this.track.Location = new System.Drawing.Point(15, 28);
            this.track.Name = "track";
            this.track.Size = new System.Drawing.Size(148, 21);
            this.track.TabIndex = 28;
            // 
            // botsLabel
            // 
            this.botsLabel.AutoSize = true;
            this.botsLabel.Location = new System.Drawing.Point(12, 67);
            this.botsLabel.Name = "botsLabel";
            this.botsLabel.Size = new System.Drawing.Size(31, 13);
            this.botsLabel.TabIndex = 29;
            this.botsLabel.Text = "Bots:";
            // 
            // botbar
            // 
            this.botbar.Location = new System.Drawing.Point(15, 83);
            this.botbar.Minimum = 1;
            this.botbar.Name = "botbar";
            this.botbar.Size = new System.Drawing.Size(148, 45);
            this.botbar.TabIndex = 30;
            this.botbar.Value = 2;
            this.botbar.ValueChanged += new System.EventHandler(this.botbar_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Botname:";
            // 
            // botName
            // 
            this.botName.Location = new System.Drawing.Point(15, 147);
            this.botName.Name = "botName";
            this.botName.Size = new System.Drawing.Size(148, 20);
            this.botName.TabIndex = 32;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(15, 192);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(148, 20);
            this.password.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Password:";
            // 
            // NewRace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(216, 297);
            this.Controls.Add(this.password);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.botName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.botbar);
            this.Controls.Add(this.botsLabel);
            this.Controls.Add(this.track);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewRace";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "NewRace";
            ((System.ComponentModel.ISupportInitialize)(this.botbar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label botsLabel;
        public System.Windows.Forms.ComboBox track;
        public System.Windows.Forms.TrackBar botbar;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox botName;
        public System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label3;

    }
}
