﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Threading;
using Newtonsoft.Json;
using System.IO;

namespace Herring
{
    public partial class Form1 : Form
    {
        BackgroundWorker plotter;
        AutoResetEvent plotAvailable = new AutoResetEvent(false);

        ConcurrentDictionary<string, ConcurrentQueue<PlotData>> plotThis = new ConcurrentDictionary<string,ConcurrentQueue<PlotData>>();
        Dictionary<string, List<PlotData>> savedPlots = new Dictionary<string, List<PlotData>>();
        ConcurrentDictionary<string, BotForm> botList = new ConcurrentDictionary<string, BotForm>();
        List<BackgroundWorker> racingBots = new List<BackgroundWorker>();

        string currentServer;

        public Form1()
        {
            InitializeComponent();
            StartPlotter();
            testServer.SelectedIndex = 1;

            botEngCombo.Items.AddRange(BotFactory.BotTypes().ToArray());
            LoadSettings();
        }

        public string MakePlayerName()
        {
            if(racingBots.Count == 0)
            {
                if (System.Environment.MachineName == "ILUNISO")
                {
                    return "HerrBott";
                }
                else
                {
                    //Nikke
                    return "AutoBott";
                }
            }
            else
            {
                if (System.Environment.MachineName == "ILUNISO")
                {
                    return string.Format("HerrBott{0}", racingBots.Count);
                }
                else
                {
                    //Nikke
                    return string.Format("AutoBott{0}", racingBots.Count);
                }
            }
        }

        void raceThread_DoWork(object sender, DoWorkEventArgs e)
        {
            RaceParams arg = e.Argument as RaceParams;

            List<string> args = new List<string>();
            args.Add(currentServer);
            args.Add("8091");
            args.Add(arg.botname);
            args.Add("bctZQwonBEeEGg");
            args.Add(arg.circuit);
            args.Add(arg.bots.ToString());
            args.Add(arg.password);
            args.Add(arg.botType);

            Bot.Exit = false;
            Bot.Main(args.ToArray());

            e.Result = arg.botname;
        }

        private void StartPlotter()
        {
            Bot.Plotter = PlotData;
            Bot.RaceStartFunc = RaceStart;
            Bot.LapCompletedFunc = LapCompleted;

            plotter = new BackgroundWorker();
            plotter.WorkerSupportsCancellation = true;
            plotter.DoWork += plotter_DoWork;
            plotter.RunWorkerAsync();
        }

        void Plot(string id, PlotData data)
        {
            BotForm outputForm = null;

            if (botList.TryGetValue(id, out outputForm))
            {
                outputForm.Plot(data);
            }
            lookAheadControl1.Plot(data.LookAhead);
        }

        void plotter_DoWork(object sender, DoWorkEventArgs e)
        {
            while(!e.Cancel)
            {
                if(plotAvailable.WaitOne(TimeSpan.FromSeconds(1)))
                {
                    foreach(var key in plotThis.Keys)
                    {
                        ConcurrentQueue<PlotData> plotQueue;
                        PlotData last = null;

                        List<PlotData> saveTo;

                        if (!savedPlots.TryGetValue(key, out saveTo))
                        {
                            saveTo = new List<PlotData>();
                            savedPlots.Add(key, saveTo);
                        }

                        if(plotThis.TryGetValue(key, out plotQueue))
                        {
                            PlotData next = null;

                            while(plotQueue.TryDequeue(out next))
                            {
                                saveTo.Add(next);
                                last = next;
                            }
                        }

                        if(last != null)
                        {
                            Plot(key, last);
                        }
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(plotter != null)
            {
                plotter.CancelAsync();
                plotter.DoWork -= plotter_DoWork;
                plotter = null;
            }

            SaveSettings();

            e.Cancel = false;
        }

        void PlotData(string botName, Car car, CarDash dash, CarPosition position, Track track, int tick, string inputMsg, SendMsg response, LookAhead lookAhead)
        {
            ConcurrentQueue<PlotData> plotQueue;

            if (!plotThis.TryGetValue(botName, out plotQueue))
            {
                plotQueue = new ConcurrentQueue<PlotData>();
                plotThis.TryAdd(botName, plotQueue);
            }

            CarDash newDash = null;

            if(dash != null)
            {
                newDash = dash.Copy();
            }

            LookAhead newLookAhead = null;
            if (lookAhead != null)
            {
                newLookAhead = lookAhead.Copy();
            }
            plotQueue.Enqueue(new PlotData(newDash, position, tick, car, inputMsg, response, newLookAhead));
            plotAvailable.Set();
        }

        string _raceName = "";
        int _lapCount = 0;

        void RaceStart(GameInit data)
        {
            if(data != null && data.race != null)
            {
                _raceName = data.race.track.name;

                if(data.race.raceSession != null)
                {
                    _lapCount = data.race.raceSession.laps;
                }

                ViewCar(data.race.cars[0]);
                trackView.SetTrack(data.race.track);
            }
        }

        delegate void ViewCarDelegate(Car car);

        public void ViewCar(Car car)
        {
            if (InvokeRequired)
            {
                Invoke(new ViewCarDelegate(ViewCar), car);
            }
            else
            {
                pegPos.Text = car.dimensions.guideFlagPosition.ToString();
                carLength.Text = car.dimensions.length.ToString();
                carWidth.Text = car.dimensions.width.ToString();
            }
        }


        void BotPosChange(CarId id, CarPosition pos, LookAhead la)
        {
            trackView.BotPosition(id, pos);
            lookAheadControl1.Plot(la);
        }

        delegate void LapCompletedDelegate(string botName, LapFinished lap);

        void LapCompleted(string botName, LapFinished lap)
        {
            if (InvokeRequired)
            {
                Invoke(new LapCompletedDelegate(LapCompleted), botName, lap);
            }
            else
            {
                BotForm outputForm = null;

                if (botList.TryGetValue(botName, out outputForm))
                {
                    outputForm.SetLapTime(lap);
                }
            }
        }

        private void Race(string track)
        {
            playerTab.TabPages.Clear();
            botList.Clear();

            string botName = MakePlayerName();

            var tab = new TabPage();
            tab.Text = botName;
            playerTab.TabPages.Add(tab);

            BotForm form = new BotForm();
            form.BotPosChange = BotPosChange;
            form.Clear();
            form.Dock = DockStyle.Fill;
            tab.Controls.Add(form);
            botList.TryAdd(botName, form);

            savedPlots.Clear();

            var raceThread = new BackgroundWorker();
            raceThread.DoWork += raceThread_DoWork;
            raceThread.RunWorkerCompleted += raceThread_RunWorkerCompleted;
            racingBots.Add(raceThread);

            raceThread.RunWorkerAsync(new RaceParams(track, botName, botEngCombo.SelectedItem as string));

            cancelToolStripMenuItem.Enabled = true;
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("keimola");
        }

        private void germanyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("germany");
        }

        private void usaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("usa");
        }

        private void franceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("france");
        }

        private void eläintarhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("elaeintarha");
        }

        private void italyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("imola");
        }

        private void englandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("england");
        }

        private void japanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Race("suzuka");
        }

        void raceThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            racingBots.Remove(sender as BackgroundWorker);

            string botName = e.Result as string;

            BotForm outputForm = null;

            if (botName != null)
            {
                if (botList.TryGetValue(botName, out outputForm))
                {
                    List<PlotData> saveTo;

                    if (savedPlots.TryGetValue(botName, out saveTo))
                    {
                        outputForm.ShowHistory(saveTo);
                    }
                }
            }

            if(racingBots.Count == 0)
            {
                advancedToolStripMenuItem.Enabled = true;
                startToolStripMenuItem.Enabled = true;
                cancelToolStripMenuItem.Enabled = false;
                germanyToolStripMenuItem.Enabled = true;
            }
            
        }

        private void cancelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bot.Exit = true;
        }

        private void raceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewRace newRace = new NewRace();
            
            if(newRace.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int bots = newRace.botbar.Value;
                string track = newRace.track.SelectedItem as string;

                string botName = MakePlayerName();
                var tab = new TabPage();
                tab.Text = botName;
                playerTab.TabPages.Add(tab);

                BotForm form = new BotForm();
                form.BotPosChange = BotPosChange;
                form.Clear();
                form.Dock = DockStyle.Fill;
                tab.Controls.Add(form);
                botList.TryAdd(botName, form);

                savedPlots.Clear();

                var raceThread = new BackgroundWorker();
                raceThread.DoWork += raceThread_DoWork;
                raceThread.RunWorkerCompleted += raceThread_RunWorkerCompleted;
                racingBots.Add(raceThread);
                raceThread.RunWorkerAsync(new RaceParams(track, botName, botEngCombo.SelectedItem as string, true, bots));

                //Dummy wait for race to be finished...
                Thread.Sleep(500);

                for(int i = 1; i < bots; i++)
                {
                    botName = MakePlayerName();
                    tab = new TabPage();
                    tab.Text = botName;
                    playerTab.TabPages.Add(tab);

                    form = new BotForm();
                    form.BotPosChange = BotPosChange;
                    form.Clear();
                    form.Dock = DockStyle.Fill;
                    tab.Controls.Add(form);
                    botList.TryAdd(botName, form);

                    raceThread = new BackgroundWorker();
                    raceThread.DoWork += raceThread_DoWork;
                    raceThread.RunWorkerCompleted += raceThread_RunWorkerCompleted;
                    racingBots.Add(raceThread);
                    raceThread.RunWorkerAsync(new RaceParams(track, botName, botEngCombo.SelectedItem as string, false, bots));
                }
            }
        }

        private void testServer_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            currentServer = testServer.SelectedItem as string;
        }

        private void createRaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewRace newRace = new NewRace();
            newRace.botName.Text = MakePlayerName();
            newRace.password.Text = System.Environment.MachineName;
            newRace.botName.Enabled = false;
            newRace.botName.Enabled = false;
            newRace.Text = "Create Race";

            if (newRace.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                playerTab.TabPages.Clear();
                botList.Clear();
                savedPlots.Clear();

                int bots = newRace.botbar.Value;
                string track = newRace.track.SelectedItem as string;
                string password = newRace.password.Text;

                for(int i = 0; i < bots; i++)
                {
                    string botName = MakePlayerName();
                    var tab = new TabPage();
                    tab.Text = botName;
                    playerTab.TabPages.Add(tab);

                    BotForm form = new BotForm();
                    form.BotPosChange = BotPosChange;
                    form.Clear();
                    form.Dock = DockStyle.Fill;
                    tab.Controls.Add(form);
                    botList.TryAdd(botName, form);

                    var raceThread = new BackgroundWorker();
                    raceThread.DoWork += raceThread_DoWork;
                    raceThread.RunWorkerCompleted += raceThread_RunWorkerCompleted;
                    racingBots.Add(raceThread);
                    raceThread.RunWorkerAsync(new RaceParams(track, botName, botEngCombo.SelectedItem as string, false, bots, password));

                    int sleepCount = 0;

                    while(Bot.joinCount == 0 && sleepCount < 5000)
                    {
                        Thread.Sleep(200);
                        sleepCount += 100;
                    }
                }

                cancelToolStripMenuItem.Enabled = true;
                advancedToolStripMenuItem.Enabled = false;
                startToolStripMenuItem.Enabled = false;
                germanyToolStripMenuItem.Enabled = false;
            }
        }

        private void joinRaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewRace newRace = new NewRace();
            newRace.botName.Text = MakePlayerName();
            newRace.password.Text = System.Environment.MachineName;
            newRace.Text = "Join Race";

            if (newRace.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                playerTab.TabPages.Clear();
                botList.Clear();
                savedPlots.Clear();

                int bots = newRace.botbar.Value;
                string track = newRace.track.SelectedItem as string;
                string botName = newRace.botName.Text;
                string password = newRace.password.Text;
                
                var tab = new TabPage();
                tab.Text = botName;
                playerTab.TabPages.Add(tab);

                BotForm form = new BotForm();
                form.BotPosChange = BotPosChange;
                form.Clear();
                form.Dock = DockStyle.Fill;
                tab.Controls.Add(form);
                botList.TryAdd(botName, form);

                var raceThread = new BackgroundWorker();
                raceThread.DoWork += raceThread_DoWork;
                raceThread.RunWorkerCompleted += raceThread_RunWorkerCompleted;
                racingBots.Add(raceThread);
                raceThread.RunWorkerAsync(new RaceParams(track, botName, botEngCombo.SelectedItem as string, false, bots, password));

                cancelToolStripMenuItem.Enabled = true;
                advancedToolStripMenuItem.Enabled = false;
                startToolStripMenuItem.Enabled = false;
                germanyToolStripMenuItem.Enabled = false;
            }
        }

        string Filename()
        {
            string path = Path.GetDirectoryName(Application.ExecutablePath) + "\\sett.json";
            return path;
        }

        public void LoadSettings()
        {
            Settings msg = null;

            try
            {
                using (FileStream str = new FileStream(Filename(), FileMode.Open, FileAccess.Read))
                {
                    StreamReader reader = new StreamReader(str);
                    string line = reader.ReadLine();

                    msg = JsonConvert.DeserializeObject<Settings>(line);
                    reader.Close();
                }
            }
            catch(Exception)
            {

            }

            if(msg != null)
            {
                for(int i = 0; i < botEngCombo.Items.Count; i++)
                {
                    if(botEngCombo.Items[i] as string == msg.botType)
                    {
                        botEngCombo.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        public void SaveSettings()
        {
            Settings msg = new Settings();
            msg.botType = botEngCombo.SelectedItem as string;

            string output = JsonConvert.SerializeObject(msg);

            using(FileStream str = new FileStream(Filename(), FileMode.Create, FileAccess.ReadWrite))
            {
                StreamWriter writer = new StreamWriter(str);
                writer.WriteLine(output);
                writer.Close();
            }
        }
    }

    public class Settings
    {
        public string botType;
    }

    public class RaceParams
    {
        public string circuit;
        public string botname;
        public bool create;
        public int bots;
        public string password;
        public string botType;

        public RaceParams(string circ, string name, string botType, bool create = false, int bots = 1, string password = "")
        {
            circuit = circ;
            botname = name;
            this.create = create;
            this.bots = bots;
            this.password = password;
            this.botType = botType;
        }
    }

    public class PlotData
    {
        CarDash _dash;
        CarPosition _position;
        int _tick;
        Car _car;
        LookAhead _lookAhead;

        public CarDash Dashboard
        {
            get
            {
                return _dash;
            }
            set
            {
                _dash = value;
            }
        }

        public LookAhead LookAhead
        {
            get
            {
                return _lookAhead;
            }
            set
            {
                _lookAhead = value;
            }
        }

        public CarPosition Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        public int Tick
        {
            get
            {
                return _tick;
            }
            set
            {
                _tick = value;
            }
        }

        public Car ThisCar
        {
            get
            {
                return _car;
            }
            set
            {
                _car = value;
            }
        }

        public string InputMsg
        {
            get;
            set;
        }

        public SendMsg Response
        {
            get;
            set;
        }

        public PlotData(CarDash dash, CarPosition position, int tick, Car car, string inputMsg, SendMsg response, LookAhead lookAhead)
        {
            _position = position;
            _dash = dash;
            _tick = tick;
            _car = car;
            _lookAhead = lookAhead;
            InputMsg = inputMsg;
            Response = response;
        }
    }
}
