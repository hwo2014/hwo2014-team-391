﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Herring
{
    partial class NewRace : Form
    {
        public NewRace()
        {
            InitializeComponent();
            track.SelectedIndex = 0;
        }

        private void botbar_ValueChanged(object sender, EventArgs e)
        {
            UpdateLabel();
        }

        void UpdateLabel()
        {
            botsLabel.Text = string.Format("Bots ({0}):", botbar.Value);
        }
    }
}
