﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Herring
{
    public partial class LookAheadControl : UserControl
    {
        public LookAheadControl()
        {
            InitializeComponent();
        }

        delegate void PlotDelegate(LookAhead data);

        public void Plot(LookAhead lookAhead)
        {
            if (this.InvokeRequired)
            {
                Invoke(new PlotDelegate(Plot), lookAhead);
            }
            else
            {
                UIPlot(lookAhead);
            }
        }

        private void UIPlot(LookAhead data)
        {
            if (data == null)
            {
                return;
            }

            listView1.Items.Clear();

            List<LookAheadItemData> datas = new List<LookAheadItemData>( data.Datas);
            foreach (var item in datas)
            {
                var listItem = listView1.Items.Add(item.TargetTrackId.ToString());
                listItem.SubItems.Add(item.Distance.ToString());
                listItem.SubItems.Add(item.TargetSpeed.ToString());
                listItem.SubItems.Add(item.TrackPieceLength.ToString());
            }
        }
    }
}
