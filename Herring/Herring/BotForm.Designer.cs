﻿namespace Herring
{
    partial class BotForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.historyList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dash = new Herring.DashBoard();
            this.SuspendLayout();
            // 
            // historyList
            // 
            this.historyList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.historyList.FullRowSelect = true;
            this.historyList.GridLines = true;
            this.historyList.Location = new System.Drawing.Point(550, 3);
            this.historyList.MultiSelect = false;
            this.historyList.Name = "historyList";
            this.historyList.Size = new System.Drawing.Size(313, 259);
            this.historyList.TabIndex = 2;
            this.historyList.UseCompatibleStateImageBehavior = false;
            this.historyList.View = System.Windows.Forms.View.Details;
            this.historyList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.historyList_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Tick";
            this.columnHeader1.Width = 43;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ServerMsg";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Response";
            this.columnHeader3.Width = 80;
            // 
            // dash
            // 
            this.dash.Dock = System.Windows.Forms.DockStyle.Left;
            this.dash.Location = new System.Drawing.Point(0, 0);
            this.dash.Name = "dash";
            this.dash.Size = new System.Drawing.Size(866, 265);
            this.dash.TabIndex = 1;
            this.dash.Load += new System.EventHandler(this.dash_Load);
            // 
            // BotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.historyList);
            this.Controls.Add(this.dash);
            this.Name = "BotForm";
            this.Size = new System.Drawing.Size(866, 265);
            this.ResumeLayout(false);

        }

        #endregion

        public DashBoard dash;
        private System.Windows.Forms.ListView historyList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;

    }
}
