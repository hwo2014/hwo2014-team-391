﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Herring
{
    public partial class BotForm : UserControl
    {
        public delegate void BotPosition(CarId car, CarPosition pos, LookAhead la);

        public BotPosition BotPosChange;

        public BotForm()
        {
            InitializeComponent();
        }

        public string PlayerId
        {
            get;
            set;
        }

        delegate void PlotDelegate(PlotData data);

        public void Plot(PlotData data)
        {
            if(this.InvokeRequired)
            {
                Invoke(new PlotDelegate(Plot), data);
            }
            else
            {
                UIPlot(data);
            }
        }

        private void UIPlot(PlotData data)
        {
            dash.View(data);

            if (BotPosChange != null)
            {
                BotPosChange(data.ThisCar.id, data.Position, data.LookAhead);
            }
        }

        private void historyList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if(e.IsSelected && e.Item != null)
            {
                PlotData d = e.Item.Tag as PlotData;

                if(d != null)
                {
                    UIPlot(d);
                }
            }
        }

        public void ShowHistory(List<PlotData> list)
        {
            foreach(var item in list)
            {
                var listItem = historyList.Items.Add(item.Tick.ToString());
                listItem.SubItems.Add(item.InputMsg);

                if(item.Response != null)
                {
                    listItem.SubItems.Add(item.Response.MsgType());
                }
                
                listItem.Tag = item;
            }
        }

        int bestTime = 0;

        public void Clear()
        {
            dash.Clear();
            dash.lapTime.Text = "";
            bestTime = 0;
            historyList.Items.Clear();
        }

        public void SetLapTime(LapFinished lap)
        {
            if (bestTime > lap.lapTime.millis || bestTime == 0)
            {
                bestTime = lap.lapTime.millis;
                dash.lapTime.Text = string.Format("{0}", lap.lapTime.millis);
            }
        }

        private void dash_Load(object sender, EventArgs e)
        {

        }
    }
}
